// This part of the system recreates the DAQ side.
// It is used primarily to test the RD-53A emulator, which
// is the product of more interest to most users.

// To send commands from a host computer, see the README.md file for instructions

module off_chip_top (
   input  rst,
   input  sysclk_in_p, sysclk_in_n, 
   input  trigger,
   input  command,
   input  [3:0] cmd_in_p, cmd_in_n,
   output clk40out,
   output htl,      // hold_while_locking_out
   output dataout_n,
   output dataout_p,
   output [7:0] led,
   input  rx,
   output tx,
   output [3:0] debug
   
);

wire clk40, clk80, clk160, clk640; 
wire trig_rdy, sync_rdy, cmd_rdy;
wire word_sent, pll_locked, rst_or_lock, dataout;
wire [15:0] trig_data, cmd_data;

reg  [15:0] ser_data_i;
reg  [ 4:0] lock_count;
reg  [ 3:0] pres_state, next_state;
reg  sync_sent_i; 
reg  trig_clr, get_cmd_i; 
reg  trig_i, trig_ii, cmd_i, cmd_ii;
reg  hold_while_locking, hold_while_locking_i;

wire [7:0] uart_rx_data, uart_tx_data;
wire uart_dv, uart_tx_dv, uart_tx_busy;
wire uart_trig, uart_cmd;
wire [63:0] rec_data;
wire rec_data_valid;

wire [7:0] cmd_ascii;

localparam lock_num     = 24;       // Check the counter size
localparam lock_send    = 4'b0001;
localparam sync_send    = 4'b0010;
localparam trig_send    = 4'b0100;
localparam cmd_send     = 4'b1000;
localparam sync_pattern = 16'b1000_0001_0111_1110;

// To identify board is operating correctly
assign led[7:0] = uart_rx_data;
assign debug[3:0] = {3'b101, tx};
   
// Generate the input 40MHz clock that is used in the sample DAQ
off_chip_pll pll_i(
   .clk_in1_p(sysclk_in_p),
   .clk_in1_n(sysclk_in_n),
   .clk_out1(clk40),
   .clk_out2(clk80),
   .clk_out3(clk160),
   .clk_out4(clk640),
   .reset(rst),
   .locked(pll_locked)
);

assign rst_or_lock = rst | !pll_locked;
assign htl = hold_while_locking;
assign clk40out = clk40;

// Input metastability
always @ (posedge clk40 or posedge rst_or_lock) begin
   if (rst_or_lock) begin
      cmd_i  <= 1'b0; cmd_ii  <= 1'b0;
   end
   else begin
      cmd_i  <= command; cmd_ii  <= cmd_i;
   end
end

always @ (posedge clk80 or posedge rst_or_lock) begin
    if (rst_or_lock) begin
        trig_i <= 1'b0; trig_ii <= 1'b0;
    end
    else begin
        trig_i <= trigger; trig_ii <= trig_i;
    end
end

// Generate Triggers
triggerunit trig_gen(
   .rst(hold_while_locking),
   .clk80(clk80),
   .clk160(clk160),
   .trigger(trig_ii | uart_trig),
   .trig_clr(trig_clr),
   .trigger_rdy(trig_rdy),
   .enc_trig(trig_data)
);

// Send Sync pattern
sync_timer sync_gen(
   .rst(hold_while_locking),
   .clk(clk160),
   .word_sent(word_sent),
   .sync_sent(sync_sent_i),
   .sync_time(sync_rdy)
);

// Command generation
cmd_top cmd_gen(
   .rst(hold_while_locking),
   .clk40(clk40),
   .clk160(clk160),
   .gen_cmd(cmd_ii | uart_cmd),
   .rd_cmd(get_cmd_i),
   .cmd_valid(cmd_rdy),
   .cmd_data(cmd_data),
   .cmd_type(cmd_ascii)
);

// Prepare TTC data
always @ (*) begin
   case (pres_state)
      lock_send: begin
         ser_data_i  = sync_pattern;
         get_cmd_i   = 1'b0;
         sync_sent_i = 1'b0;
         trig_clr    = 1'b0;
         hold_while_locking_i = 1'b1;
         if (lock_count >= lock_num) begin
            next_state = sync_send;
         end
         else begin
            next_state = lock_send;
         end
      end
      sync_send: begin
         ser_data_i = sync_pattern;
         get_cmd_i  = 1'b0;
         trig_clr   = 1'b0;
         hold_while_locking_i = 1'b0;
         if (trig_rdy) begin
            next_state  = trig_send;
            sync_sent_i = 1'b0;
         end
         else if (cmd_rdy & !sync_rdy) begin
            next_state  = cmd_send;
            sync_sent_i = 1'b0;
         end
         else if (word_sent) begin
            next_state  = sync_send;
            sync_sent_i = 1'b1;
         end
         else begin
            next_state  = sync_send;
            sync_sent_i = 1'b0;
         end
      end
      trig_send: begin
         ser_data_i  = trig_data;
         get_cmd_i   = 1'b0;
         sync_sent_i = 1'b0;
         hold_while_locking_i = 1'b0;
         if (word_sent) begin
            next_state = sync_send;
            trig_clr    = 1'b1;
         end
         else begin
            next_state = trig_send;
            trig_clr    = 1'b0;
         end
      end
      cmd_send: begin
         ser_data_i  = cmd_data;
         sync_sent_i = 1'b0;
         trig_clr    = 1'b0;
         hold_while_locking_i = 1'b0;
         if (trig_rdy) begin
            get_cmd_i  = 1'b0;
            next_state = trig_send;
         end
         else if (sync_rdy) begin
            get_cmd_i  = 1'b0;
            next_state = sync_send;
         end
         else if (word_sent) begin
            get_cmd_i  = 1'b1;
            next_state = sync_send;
         end
         else begin
            get_cmd_i  = 1'b0;
            next_state = cmd_send;
         end
      end
      default: begin
         ser_data_i  = sync_pattern;
         next_state  = sync_send;
         get_cmd_i   = 1'b0;
         sync_sent_i = 1'b0;
         trig_clr    = 1'b0;
         hold_while_locking_i = 1'b0;
      end
   endcase
end

always @ (posedge clk160 or posedge rst_or_lock) begin
   if (rst_or_lock) begin
      pres_state <= lock_send; 
      lock_count <= 5'h00;
      hold_while_locking <= 1'b1;
   end
   else begin
      pres_state <= next_state;
      hold_while_locking <= hold_while_locking_i;
      if (word_sent) begin
         lock_count <= lock_count + 1;
      end
      else begin
         lock_count <= lock_count;
      end
   end
end

// Serializes TTC data
SER serializer(
   .rst(rst_or_lock),
   .clk(clk160),
   .datain(ser_data_i),
   .next(word_sent),
   .dataout(dataout)
);

// Make TTC a differential signal
OBUFDS OBUFDS_i(
    .O(dataout_p),
    .OB(dataout_n),
    .I(dataout)
);

// UART RX block
uart_rx uart_rx_i (
    .clk(clk40),
    .RxD(rx),
    .RxD_data_ready(uart_dv),
    .RxD_data(uart_rx_data), 
    .RxD_idle(),
    .RxD_endofpacket() 
);

uart_tx uart_tx_i(
    .clk(clk40),
    .TxD_start(uart_cmd),
    .TxD_data(cmd_ascii),
    .TxD(tx),
    .TxD_busy(uart_tx_busy)
);

uart_parser parse_uart_i(   
    .clk40(clk40),
    .rst(rst),
    .data_valid(uart_dv),
    .data(uart_rx_data),
    .trigger(uart_trig),
    .command(uart_cmd)
);

rd53_rx rx_core_i (
    .clk625(clk640),
    .clk156p25(clk160),
    .rst(rst),
    .data(rec_data),
    .data_valid(rec_data_valid),
    .data_in_p(cmd_in_p),
    .data_in_n(cmd_in_n)
);

// Instantiate ILA to test signals back to ~DAQ
/*
view_data view_emulator_output (
	.clk(clk160), 

	.probe0(rec_data), 
	.probe1(rec_data_valid) 
);
*/

endmodule
