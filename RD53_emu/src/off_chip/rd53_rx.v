module rd53_rx (
    input clk625,
    input clk156p25,
    input rst,
    output [63:0] data,
    output service,
    output reg data_valid,
    input [3:0] data_in_p,
    input [3:0] data_in_n,
    input bitslip
);

reg [31:0] data32;

wire [7:0] sipo0, sipo1, sipo2, sipo3;
wire [65:0] data66;
wire gearbox_data_valid;
wire [63:0] data_descrambled;
wire [1:0] sync_info;
    
cmd_iserdes i0 (
    .data_in_from_pins_p(data_in_p[0]),
    .data_in_from_pins_n(data_in_n[0]),
    .clk_in(clk625),
    .clk_div_in(clk156p25),
    .io_reset(rst),
    .bitslip(bitslip),
    .data_in_to_device(sipo0)
);

cmd_iserdes i1 (
    .data_in_from_pins_p(data_in_p[1]),  
    .data_in_from_pins_n(data_in_n[1]),  
    .clk_in(clk625),                            
    .clk_div_in(clk156p25),                    
    .io_reset(rst),                        
    .bitslip(bitslip),                          
    .data_in_to_device(sipo1)      
);

cmd_iserdes i2 (
    .data_in_from_pins_p(data_in_p[2]),  
    .data_in_from_pins_n(data_in_n[2]),  
    .clk_in(clk625),                            
    .clk_div_in(clk156p25),                    
    .io_reset(rst),                        
    .bitslip(bitslip),                          
    .data_in_to_device(sipo2)      
);

cmd_iserdes i3 (
    .data_in_from_pins_p(data_in_p[3]),  
    .data_in_from_pins_n(data_in_n[3]),  
    .clk_in(clk625),                            
    .clk_div_in(clk156p25),                    
    .io_reset(rst),                        
    .bitslip(bitslip),                          
    .data_in_to_device(sipo3)      
);

// Select and concatenate the data from the ISerDes
//integer i;  
//always @(*) begin 
//    for (i = 0; i < 8; i = i + 1) begin
//        data32[4 * i + 0] = sipo0[i];
//        data32[4 * i + 1] = sipo1[i];
//        data32[4 * i + 2] = sipo2[i];
//        data32[4 * i + 3] = sipo3[i];
//    end
//end

always @(*) begin
    data32 = {sipo0, sipo1, sipo2, sipo3};
end

gearbox32to66 gearbox_i (
    .rst(rst),
    .clk(clk156p25),
    .data32(data32),
    .data66(data66),
    .data_valid(gearbox_data_valid)
);

descrambler descramble_i (
    .data_in(data66),
    .enable(gearbox_data_valid),
    .clk(clk156p25),
    .rst(rst),
    .data_out(data_descrambled),
    .sync_info(sync_info)
);

assign data = data_descrambled;
// if the sync_info is 2'b10, we have a sync frame
// simplify to one bit on output
assign service = (sync_info == 2'b10 ? 1'b1 : 1'b0);

always @(posedge clk156p25 or posedge rst)
    if (rst)
        data_valid <= 1'b0;
    else
        data_valid <= gearbox_data_valid;

endmodule
