onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Top /single_serdes_tb/clk40
add wave -noupdate -expand -group Top /single_serdes_tb/clk160
add wave -noupdate -expand -group Top /single_serdes_tb/clk640
add wave -noupdate -expand -group Top /single_serdes_tb/rxgearboxslip_out
add wave -noupdate -expand -group Top /single_serdes_tb/bitslip_cnt
add wave -noupdate -expand -group Top /single_serdes_tb/serdes_cnt
add wave -noupdate -expand -group Top /single_serdes_tb/data32
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} /single_serdes_tb/piso0_1280/io_reset
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} /single_serdes_tb/data32
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} /single_serdes_tb/piso
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o0 -color {Orange Red} /single_serdes_tb/piso0_1280/data_out_to_pins_p
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o0 -color {Orange Red} /single_serdes_tb/piso0_1280/data_out_to_pins_n
add wave -noupdate -expand -group Top -expand -group ISERDES -color Gold /single_serdes_tb/i0/io_reset
add wave -noupdate -expand -group Top -expand -group ISERDES -color Gold /single_serdes_tb/data32_iserdes
add wave -noupdate -expand -group Top -expand -group ISERDES -color Gold /single_serdes_tb/sipo
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i0 -color Gold /single_serdes_tb/i0/data_in_from_pins_p
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i0 -color Gold /single_serdes_tb/i0/data_in_from_pins_n
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {10023370 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 233
configure wave -valuecolwidth 227
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 fs} {258808600 fs}
