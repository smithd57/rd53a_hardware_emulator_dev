// Testbench for the IO Buffer Driver
// Author: Lev Kurilenko
// Date: 11/29/2017

`timescale 1ns/1ps

module io_buf_config_driver_tb ();

parameter clk160_period = 6.25;
integer i = 0;

reg rst;
reg clk160;
reg [31:0] io_config;
reg start;

//reg ser_out;

reg [31:0] shift_reg;
reg [31:0] output_reg;

wire latch;
wire clk_io;
wire ser_in;

io_buf_config_driver io_buf_config(
    .rst(rst),
    .clk160(clk160),
    .io_config(io_config),
    .start(start),
    
    .latch(latch),
    .clk_io(clk_io),
    .ser_in(ser_in)
);

initial begin
    rst             <= 1'b1;
    clk160          <= 1'b1;
    io_config       <= 32'h0000_0000;
    start           <= 1'b0;
    
    shift_reg       <= 32'h0000_0000;
    output_reg      <= 32'h0000_0000;
    //ser_out         <= 1'b1;
    
    i <= ~0;
end

// 160 MHz clock
always #(clk160_period/2) begin
    clk160 <= ~clk160;
end

always @(posedge clk_io) begin
    shift_reg <= {shift_reg[30:0], ser_in};
end

always @(posedge latch) begin
    output_reg <= shift_reg;
end

initial begin
    repeat (3) @(posedge clk160);
    rst             <= 1'b0;
    io_config       <= 32'hAAAF_AAAF;
    start           <= 1'b1;
    @(posedge clk160);
    start           <= 1'b0;
    repeat (400) @(posedge clk160);
    start           <= 1'b1;
    io_config       <= 32'hC0CA_C01A;
    @(posedge clk160);
    start           <= 1'b0;
    repeat (1000) @(posedge clk160);
    $stop;
end    

endmodule
