onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Top /system_test_serdes_tb/clk160
add wave -noupdate -expand -group Top /system_test_serdes_tb/clk640
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_serdes_tb/scr/data_in
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_serdes_tb/scr/data_out
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_serdes_tb/scr/poly
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_serdes_tb/scr/scrambler
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_serdes_tb/scr/enable
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /system_test_serdes_tb/scr/rst
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold -radix binary /system_test_serdes_tb/scr/sync_info
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_serdes_tb/tx_gb/data32
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_serdes_tb/tx_gb/data66
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} -radix decimal /system_test_serdes_tb/tx_gb/counter
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_serdes_tb/tx_gb/data_next
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_serdes_tb/tx_gb/rst
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_serdes_tb/tx_gb/gearbox_en
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_serdes_tb/tx_gb/gearbox_rdy
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /system_test_serdes_tb/tx_gb/buffer_132
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} /system_test_serdes_tb/piso0_1280/io_reset
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} /system_test_serdes_tb/piso1_1280/io_reset
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} /system_test_serdes_tb/piso2_1280/io_reset
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} /system_test_serdes_tb/piso3_1280/io_reset
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} -radix hexadecimal /system_test_serdes_tb/piso0
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} -radix hexadecimal /system_test_serdes_tb/piso1
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} -radix hexadecimal /system_test_serdes_tb/piso2
add wave -noupdate -expand -group Top -expand -group OSERDES -color {Orange Red} -radix hexadecimal /system_test_serdes_tb/piso3
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o0 -color {Orange Red} /system_test_serdes_tb/piso0_1280/data_out_to_pins_p
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o0 -color {Orange Red} /system_test_serdes_tb/piso0_1280/data_out_to_pins_n
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o1 -color {Orange Red} /system_test_serdes_tb/piso1_1280/data_out_to_pins_p
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o1 -color {Orange Red} /system_test_serdes_tb/piso1_1280/data_out_to_pins_n
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o2 -color {Orange Red} /system_test_serdes_tb/piso2_1280/data_out_to_pins_p
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o2 -color {Orange Red} /system_test_serdes_tb/piso2_1280/data_out_to_pins_n
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o3 -color {Orange Red} /system_test_serdes_tb/piso3_1280/data_out_to_pins_p
add wave -noupdate -expand -group Top -expand -group OSERDES -expand -group o3 -color {Orange Red} /system_test_serdes_tb/piso3_1280/data_out_to_pins_n
add wave -noupdate -expand -group Top -expand -group ISERDES -color {Forest Green} /system_test_serdes_tb/i0/io_reset
add wave -noupdate -expand -group Top -expand -group ISERDES -color {Forest Green} /system_test_serdes_tb/i1/io_reset
add wave -noupdate -expand -group Top -expand -group ISERDES -color {Forest Green} /system_test_serdes_tb/i2/io_reset
add wave -noupdate -expand -group Top -expand -group ISERDES -color {Forest Green} /system_test_serdes_tb/i3/io_reset
add wave -noupdate -expand -group Top -expand -group ISERDES -color {Forest Green} /system_test_serdes_tb/data32_iserdes
add wave -noupdate -expand -group Top -expand -group ISERDES -color {Forest Green} -radix hexadecimal /system_test_serdes_tb/sipo0
add wave -noupdate -expand -group Top -expand -group ISERDES -color {Forest Green} -radix hexadecimal /system_test_serdes_tb/sipo1
add wave -noupdate -expand -group Top -expand -group ISERDES -color {Forest Green} -radix hexadecimal /system_test_serdes_tb/sipo2
add wave -noupdate -expand -group Top -expand -group ISERDES -color {Forest Green} -radix hexadecimal /system_test_serdes_tb/sipo3
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i0 -color {Forest Green} /system_test_serdes_tb/i0/data_in_from_pins_p
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i0 -color {Forest Green} /system_test_serdes_tb/i0/data_in_from_pins_n
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i1 -color {Forest Green} /system_test_serdes_tb/i1/data_in_from_pins_p
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i1 -color {Forest Green} /system_test_serdes_tb/i1/data_in_from_pins_n
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i2 -color {Forest Green} /system_test_serdes_tb/i2/data_in_from_pins_p
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i2 -color {Forest Green} /system_test_serdes_tb/i2/data_in_from_pins_n
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i3 -color {Forest Green} /system_test_serdes_tb/i3/data_in_from_pins_p
add wave -noupdate -expand -group Top -expand -group ISERDES -expand -group i3 -color {Forest Green} /system_test_serdes_tb/i3/data_in_from_pins_n
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_serdes_tb/rx_gb/data32
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_serdes_tb/rx_gb/data66
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} -radix decimal /system_test_serdes_tb/rx_gb/counter
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_serdes_tb/rx_gb/data_valid
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_serdes_tb/rx_gb/rst
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_serdes_tb/rx_gb/gearbox_en
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_serdes_tb/rx_gb/gearbox_rdy
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_serdes_tb/rx_gb/buffer_128
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /system_test_serdes_tb/rx_gb/buffer_pos
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_serdes_tb/uns/data_in
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_serdes_tb/uns/data_out
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_serdes_tb/uns/poly
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_serdes_tb/uns/descrambler
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_serdes_tb/uns/enable
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /system_test_serdes_tb/uns/rst
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange -radix binary /system_test_serdes_tb/uns/sync_info
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_serdes_tb/b_sync/system_reset
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_serdes_tb/b_sync/blocksync_out
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_serdes_tb/b_sync/rxgearboxslip_out
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_serdes_tb/b_sync/rxheader_in
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_serdes_tb/b_sync/rxheadervalid_in
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_serdes_tb/b_sync/sync_header_count_i
add wave -noupdate -expand -group Top -expand -group {Block Sync} -color {Slate Blue} /system_test_serdes_tb/b_sync/sync_header_invalid_count_i
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {6250 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 233
configure wave -valuecolwidth 227
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {4140938 ps}
