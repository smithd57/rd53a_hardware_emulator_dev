module clock_data_recovery(
	input logic clkx8, 
	input logic data_i, 
	input logic rst_i,
	output logic data_o,
	output logic clk_o);

//check for positive edges
logic up_pulse, feedback_up;
always_ff @(posedge data_i) begin
	if (rst_i | feedback_up)
		up_pulse <= 0;
	else
		up_pulse <= 1;
end

assign feedback_up = ~up_pulse;

//check for negative edges
logic down_pulse, feedback_down, data_not;
assign data_not = ~data_i;

always_ff @(posedge data_not) begin
	if (rst_i | feedback_down)
		down_pulse <= 0;
	else
		down_pulse <= 1;
end

assign feedback_down <= ~down_pulse;

logic pulse;
//if an edge comes in pulse
assign pulse = ~(feedback_down & feedback_up)

//clk counter
logic [2:0] clk_count;
always_ff @(posedge clkx8) begin
	if (rst_i | pulse)
		clk_count <= 0;
	else
		clk_count <= clk_count + 1;
end

//clk and data out manager
always_ff @(posedge clkx8) begin
	if (rst_i) begin
		clk_o <= 1;
		data_o <= 0;
	end else if(pulse) begin
		clk_o <= 1;
		data_o <= data_o;
	end else if(clk_count == 4) begin
		clk_count <= 1
		data_o <= data_i;
	end
end


