
// Bloom Filter built off of 3 seeded SAX hash functions.
// Outputs whether or not the address of the given region has been seen by the filter before, "write" adds the region address to the seen list.
// Uses a RAM that can't be reset, so "clear" actually uses a timestamp system instead.
module BloomFilter #(
		     parameter STAMP_WIDTH = 4,
		     DEPTH = 256
		     ) (
		    input logic clk, rst, write, clear,
		    input logic [31:0] region,
		    output logic seen
		    );
   localparam ADDR_WIDTH = $clog2(DEPTH);
   //logic [STAMP_WIDTH-1:0] 	 timestamp;
   logic /*[STAMP_WIDTH-1:0]*/ 	 mem [DEPTH-1:0];

   logic [ADDR_WIDTH-1:0] hash1, hash2, hash3;
   SAXHash16bit #(ADDR_WIDTH) hashFunc1 (.data(region[31:16]), .seed(8'd0  ), .hash(hash1));
   SAXHash16bit #(ADDR_WIDTH) hashFunc2 (.data(region[31:16]), .seed(8'd723), .hash(hash2));
   SAXHash16bit #(ADDR_WIDTH) hashFunc3 (.data(region[31:16]), .seed(8'd497), .hash(hash3));

   logic seen1, seen2, seen3;
   always_comb begin
      /*
      seen1 = (mem[hash1] == timestamp);
      seen2 = (mem[hash2] == timestamp);
      seen3 = (mem[hash3] == timestamp);
       */
      {seen1, seen2, seen3} = {mem[hash1], mem[hash2], mem[hash3]};
      seen = seen1 & seen2 & seen3;
   end
   
   always_ff @(posedge clk) begin
      if (write) begin
	 /*
	 mem[hash1] <= timestamp;
	 mem[hash2] <= timestamp;
	 mem[hash3] <= timestamp;
	  */
	 {mem[hash1], mem[hash2], mem[hash3]} <= 3'b111;
      end

      if (rst | clear) begin
	 mem <= '{DEPTH{'0}}; // all zeros
      end
      /*
      if (rst) begin
	 timestamp <= '0;
      end else if (clear) begin
	 timestamp <= timestamp + 1;
      end else begin
	 timestamp <= timestamp;
      end
       */
   end
endmodule
