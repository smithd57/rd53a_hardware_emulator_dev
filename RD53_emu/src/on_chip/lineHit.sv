
// Similar interface to clusterHit, but generates horizontal and vertical lines instead of clusters, and
//  runs on a 2x clock to give 2 regions at once, bypassing the need for a hit FIFO buffer.
// Frequency of clk must be 2x readout clock rate with synchronization to the readout clock.
// Start signal must be released while the readout clock is high (after posedge, before negedge).
//  After the start signal is released, you may read the regions at the positive edge of the readout clock.
// The first regions readout will be {32'X, firstValidRegion}. All readouts after that will be exactly 2 valid regions.
module LineHit (
      input logic clk, rst, start,
      input logic [27:0] randin, // 9-bit x, 9-bit y, 3-bit orientation, 4-bit generator choice (16 choices)
      output logic [63:0] regions,
      output logic done
   );
   logic lineType, hdone, vdone, hrst, vrst, whichRegion, rstDelay;
   logic [31:0] hregion, vregion, lastRegion, nlastRegion;

   VLine vline (.clk, .rst(rst | rstDelay | vrst), .x(randin[8:0]), .y(randin[17:9]), .length(randin[26:18]), .region(vregion), .done(vdone));
   HLine hline (.clk, .rst(rst | rstDelay | hrst), .x(randin[8:0]), .y(randin[17:9]), .length(randin[26:18]), .region(hregion), .done(hdone));
   
   always_comb begin
      hrst = 1;
      vrst = 1;
      if (lineType) begin
	 // H-line
	 hrst = start;
	 done = hdone;
	 nlastRegion = hregion;
	 if (whichRegion) regions = {hregion, lastRegion};
	 else             regions = {lastRegion, hregion};
      end else begin
	 // V-line
	 vrst = start;
	 done = vdone;
	 nlastRegion = vregion;
	 if (whichRegion) regions = {vregion, lastRegion};
	 else             regions = {lastRegion, vregion};
      end
   end
   
   always_ff @(posedge clk) begin
      rstDelay <= rst;
      lastRegion <= nlastRegion;
      if (rst) begin
	 lineType <= 0;
	 whichRegion <= 0;
      end else if (done & start) begin
	 // Start a new line
	 lineType <= randin[27];
	 whichRegion <= 0;
      end else begin
	 whichRegion <= ~whichRegion;
      end
   end
endmodule


// Length between 4 and 192
// starts at x,y and increments y up to y+length (so length is actually one less than the line length)
// regionValid = ~done
// reset to load new values
// always reads out an odd number of regions. Pads (if necessary) with a region after the end of the line with ToTs = PAD_HIT
module VLine #(X_MAX=400, Y_MAX=192, HIT=4'hC, PAD_HIT=4'h0) (
	  input logic clk, rst,
	  input logic [8:0] x, y, length,
	  output logic [31:0] region,
	  output logic done
       );
   logic [8:0] curX, curY, stop, nextStop, limitedLength;
   logic regionParity, finishedLine; // for guaranteeing odd # of regions
   logic [15:0] clusterToT;

   // Prevents line len (length+1) from going out of bounds [4:Y_MAX], then adds it to y
   always_comb begin
      if (length < Y_MAX) begin
	 if (length < 4) limitedLength = 9'd4;
	 else            limitedLength = length;
      end else begin
	 limitedLength = Y_MAX - 1;
      end
      if ((y + limitedLength) < Y_MAX) begin
	 nextStop = (y + limitedLength);
      end else begin
	 nextStop = Y_MAX - 1;
      end
   end

   
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX), .HIT(HIT)) vlineClust1x1 (.x(curX), .y(curY), .region({region[31:16], clusterToT}));

   assign region[15:0] = finishedLine ? PAD_HIT : clusterToT;
   assign finishedLine = rst | (curY == (stop + 9'd1)) | (curY == (stop + 9'd2));
   assign done = finishedLine & regionParity;
   
   always_ff @(posedge clk) begin
      if (rst) begin
	 regionParity <= 0;
	 curX <= x;
	 stop <= nextStop;
	 // WARNING: optimized for X_MAX=400, Y_MAX=192
	 // Optimization of curY <= y % (Y_MAX - 4)
	 if ((y + 9'd4) < Y_MAX) begin
	    curY <= y;
	 end else if ((y + 9'd4) < 2*Y_MAX) begin
	    curY <= y - Y_MAX;
	 end else begin
	    curY <= y - 2*Y_MAX;
	 end
      end else begin
	 if (~done) begin
	    regionParity <= ~regionParity;
	    curY <= curY + 9'd1;
	    assert(~(^region === 1'bX)); // error if any bits unknown
	 end
      end
   end
endmodule


// Length between 4 and 400
// starts at x,y and increments x up to x+length (so length is actually one less than the line length)
// regionValid = ~done
// reset to load new values
// always reads out an odd number of regions. Pads (if necessary) with a region after the end of the line with ToTs = PAD_HIT
module HLine #(X_MAX=400, Y_MAX=192, HIT=4'hC, PAD_HIT=4'h0) (
	  input logic clk, rst,
	  input logic [8:0] x, y, length,
	  output logic [31:0] region,
	  output logic done
       );
   logic [8:0] curX, curY, stop, diff, nextStop, limitedLength;
   logic regionParity, finishedLine; // for guaranteeing odd # of regions


   // location of x inside the region
   logic [1:0] subAddr;
   XY2RegionAddress #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) coords (.x(curX), .y(curY), .addr(region[31:16]), .subAddr);

   assign done = finishedLine & regionParity;
   
   logic [3:0] totTmp, tot;
   always_comb begin
      finishedLine = rst | (curX == (stop + 9'd4));
      diff = stop - curX;

      // Prevents line len (length+1) from going out of bounds [4:X_MAX]
      if (length < X_MAX) begin
	 if (length < 4) limitedLength = 9'd4;
	 else            limitedLength = length;
      end else begin
	 limitedLength = X_MAX - 1;
      end
      if ((x + limitedLength) < X_MAX) begin
	 nextStop = (x + limitedLength);
      end else begin
	 nextStop = X_MAX - 1;
      end

      case (subAddr)
	2'd0: totTmp = 4'b1111;
	2'd1: totTmp = 4'b0111;
	2'd2: totTmp = 4'b0011;
	2'd3: totTmp = 4'b0001;
      endcase // case (subAddr)
      if (diff == 0) begin
	 tot = totTmp & 4'b1000;
      end else if (diff == 1) begin
	 tot = totTmp & 4'b1100;
      end else if (diff == 2) begin
	 tot = totTmp & 4'b1110;
      end else begin // diff > 2
	 tot = totTmp;
      end

      if (finishedLine) begin
	 region[15:0] = PAD_HIT;
      end else begin
	 region[15:0] = {tot[3] ? HIT : 4'h0,
			 tot[2] ? HIT : 4'h0,
			 tot[1] ? HIT : 4'h0,
			 tot[0] ? HIT : 4'h0};
      end
   end

   always_ff @(posedge clk) begin
      if (rst) begin
	 regionParity <= 0;
	 // WARNING: optimized for X_MAX=400, Y_MAX=192
	 // Optimization of curX <= x % (X_MAX - 4)
	 if ((x + 9'd4) < X_MAX) begin
	    curX <= x;
	 end else begin
	    curX <= x - X_MAX;
	 end
	 curY <= y;
	 stop <= nextStop;
      end else if (~done) begin
	 if ((diff > 3) & ~finishedLine) begin
	    regionParity <= ~regionParity;
	    // Go to the start of the next region
	    curX <= curX + 9'd4 - {7'd0, subAddr};
	 end else begin
	    // Last generated region being clocked out and/or padding region clocked out
	    if (~done) regionParity <= ~regionParity;
	    curX <= (stop + 9'd4);
	 end
	 assert(!(^region === 1'bX)); // error if any bits unknown
      end
   end
endmodule
