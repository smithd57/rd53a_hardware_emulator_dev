
// Generate 35 clusters of random shape at random x,y,orientation and send the regions out every clock cycle
// Usage: assert start signal for one clock cycle, then region will be a valid generated region every clock cycle while done is false.
// Once done is true, it has returned the regions corresponding to all the clusters it generated, you may assert the start signal again when ready.
module ClusterHit (
   input logic clk, rst, start, // 40 MHz = hclk, fifo reads at 160 MHz = dclk
   input logic [27:0] randin, // 9-bit x, 9-bit y, 3-bit orientation, 4-bit generator choice (16 choices)
   output logic [31:0] region,
   output logic done
);
   logic newRandom;
   logic [27:0] random;
   
   // 10 different region generators, each with up to 5 regions, each region being 32-bits
   logic [9:0][4:0][31:0] regions;
   // the masks telling which regions in a region generator are invalid 
   logic [9:0][4:0] regionMasks;
   // 0-bit orientation (1)
   Cluster1x1 cluster1x1 (.x(random[8:0]), .y(random[17:9]), .region(regions[0][0]));
   assign regions[0][4:1] = '0;
   assign regionMasks[0] = ~5'd1;
   // 2-bit orientation (6)
   Cluster2x1 cluster2x1 (.x(random[8:0]), .y(random[17:9]), .orientation(random[19:18]), .regionA(regions[1][0]), .regionB(regions[1][1]), .ignoreB(regionMasks[1][1]));
   assign regions[1][4:2] = '0;
   assign {regionMasks[1][4:2], regionMasks[1][0]} = ~4'd1;
   Cluster3x1 cluster3x1 (.x(random[8:0]), .y(random[17:9]), .orientation(random[19:18]), .regionA(regions[2][0]), .regionB(regions[2][1]), .regionC(regions[2][2]), .ignoreB(regionMasks[2][1]), .ignoreC(regionMasks[2][2]));
   assign regions[2][4:3] = '0;
   assign {regionMasks[2][4:3], regionMasks[2][0]} = ~3'd1;
   Cluster2x2_1z cluster2x2 (.x(random[8:0]), .y(random[17:9]), .orientation(random[19:18]), .regionA(regions[3][0]), .regionB(regions[3][1]), .regionC(regions[3][2]), .ignoreB(regionMasks[3][1]), .ignoreC(regionMasks[3][2]));
   assign regions[3][4:3] = '0;
   assign {regionMasks[3][4:3], regionMasks[3][0]} = ~3'd1;
   ClusterT clusterT (.x(random[8:0]), .y(random[17:9]), .orientation(random[19:18]), .regionA(regions[4][0]), .regionB(regions[4][1]), .regionC(regions[4][2]), .regionD(regions[4][3]), .ignoreB(regionMasks[4][1]), .ignoreC(regionMasks[4][2]), .ignoreD(regionMasks[4][3]));
   assign regions[4][4] = '0;
   assign {regionMasks[4][4], regionMasks[4][0]} = ~2'b01;
   ClusterZ clusterZ (.x(random[8:0]), .y(random[17:9]), .orientation(random[19:18]), .regionA(regions[5][0]), .regionB(regions[5][1]), .regionC(regions[5][2]), .regionD(regions[5][3]), .ignoreB(regionMasks[5][1]), .ignoreC(regionMasks[5][2]), .ignoreD(regionMasks[5][3]));
   assign regions[5][4] = '0;
   assign {regionMasks[5][4], regionMasks[5][0]} = ~2'b01;
   ClusterO clusterO (.x(random[8:0]), .y(random[17:9]), .orientation(random[19:18]), .regionA(regions[6][0]), .regionB(regions[6][1]), .regionC(regions[6][2]), .regionD(regions[6][3]), .ignoreB(regionMasks[6][1]), .ignoreC(regionMasks[6][2]), .ignoreD(regionMasks[6][3]));
   assign regions[6][4] = '0;
   assign {regionMasks[6][4], regionMasks[6][0]} = ~2'b01;
   // 3-bit orientation (3)
   ClusterL clusterL (.x(random[8:0]), .y(random[17:9]), .orientation(random[20:18]), .regionA(regions[7][0]), .regionB(regions[7][1]), .regionC(regions[7][2]), .regionD(regions[7][3]), .ignoreB(regionMasks[7][1]), .ignoreC(regionMasks[7][2]), .ignoreD(regionMasks[7][3]));
   assign regions[7][4] = '0;
   assign {regionMasks[7][4], regionMasks[7][0]} = ~2'b01;
   ClusterP clusterP (.x(random[8:0]), .y(random[17:9]), .orientation(random[20:18]), .regionA(regions[8][0]), .regionB(regions[8][1]), .regionC(regions[8][2]), .regionD(regions[8][3]), .regionE(regions[8][4]), .ignoreB(regionMasks[8][1]), .ignoreC(regionMasks[8][2]), .ignoreD(regionMasks[8][3]), .ignoreE(regionMasks[8][4]));
   assign regionMasks[8][0] = ~1'b1;
   ClusterF clusterF (.x(random[8:0]), .y(random[17:9]), .orientation(random[20:18]), .regionA(regions[9][0]), .regionB(regions[9][1]), .regionC(regions[9][2]), .regionD(regions[9][3]), .regionE(regions[9][4]), .ignoreB(regionMasks[9][1]), .ignoreC(regionMasks[9][2]), .ignoreD(regionMasks[9][3]), .ignoreE(regionMasks[9][4]));
   assign regionMasks[9][0] = ~1'b1;

   logic [3:0] generator;
   logic [2:0] regionLetter, nextRegionLetter, regionLetterChosen;
   assign region = regions[generator][regionLetter];
   logic [5:0] remainingClusters, nextRemainingClusters, newRemainingClusters;
   // average of 2.1875 hits/cluster
   // 192*400*0.001 = 76.8 hits/event
   // 76.8 / 2.1875 = 35 clusters/event
   // NOTE: adjust 'newRemainingClusters' to change the number of clusters generated per event
   assign newRemainingClusters = 6'd34; // a hit always has at least 1, so this is how many more regions we do after the first one
   // set generator based off of random values
   // NOTE: adjust the mapping of random values to change the frequencies associated with each cluster shape
   always_comb begin
      if      (random[26:21]  < 6'd28) generator = 4'd0; // 1x1 ~41%
      else if (random[26:21]  < 6'd40) generator = 4'd1; // 2x1 ~17%
      else if (random[26:21]  < 6'd46) generator = 4'd3; // 2x2_1z ~9%
      else if (random[26:21]  < 6'd51) generator = 4'd2; // 3x1 ~7%
      else if (random[26:21]  < 6'd54) generator = 4'd7; // L ~4%
      else if (random[26:21]  < 6'd57) generator = 4'd4; // T ~3%
      else if (random[26:21]  < 6'd60) generator = 4'd5; // Z ~3%
      else if (random[26:21]  < 6'd62) generator = 4'd8; // P ~2%
      else if (random[26:21] == 6'd62) generator = 4'd6; // O ~1%
      else if (random[26:21] == 6'd63) generator = 4'd9; // F ~1%
      else assert(^random[26:21] === 1'bX); // error if we're here despite random[26:21] being defined

   end
   

   // Give the letter corresponding to the next valid region to report, or give the same letter that came in if there are no more valid letters left
   NextLetter letterChooser (.currentLetter(regionLetter), .letterMask(regionMasks[generator]), .nextLetter(regionLetterChosen));
   
   
   localparam A = 3'd0; localparam B = 3'd1; localparam C = 3'd2;
   localparam D = 3'd3; localparam E = 3'd4;
   enum {RESET, IDLE, WORK} ps, ns;
   always_comb begin
      // Default not done
      done = 1'b0;
      // Default hold random value
      newRandom = 1'b0;
      // Default hold the number of remaining clusters to send
      nextRemainingClusters = remainingClusters;
      nextRegionLetter = 3'dX;

      case (ps)
	IDLE: begin
	   done = 1'b1;
	   newRandom = 1'b1;
	   nextRemainingClusters = newRemainingClusters;
	   nextRegionLetter = A;
	   ns = start ? WORK : IDLE;
	end
	WORK: begin
	   if (regionLetterChosen == regionLetter) begin // no new valid region, done sending this cluster
	      if (remainingClusters == 6'd0) begin // done sending all clusters
		 nextRemainingClusters = 6'dX;
		 ns = IDLE;
	      end else begin // send a new cluster
		 newRandom = 1'b1;
		 nextRegionLetter = A;
		 nextRemainingClusters = remainingClusters - 6'd1;
		 ns = WORK;
	      end
	   end else begin // stil sending this cluster, send next region
	      nextRegionLetter = regionLetterChosen;
	      ns = WORK;
	   end
	end
      endcase
   end

   always_ff @(posedge clk) begin
      remainingClusters <= nextRemainingClusters;
      regionLetter <= nextRegionLetter;
      
      if (rst) begin
	 ps <= IDLE;
	 random <= '0;
      end else begin
	 ps <= ns;
	 random <= newRandom ? randin : random;    
      end
   end
endmodule   

// Help send regions A, B, C, D, & E in that order, ending when all the remaining regions are invalid. End is signalled by nextLetter = currentLetter.
// This assumes you always start in the process of already sending region A.
module NextLetter (
		   input logic [2:0]  currentLetter,
		   input logic [4:0]  letterMask, // these are the ignore signals for each region letter
		   output logic [2:0] nextLetter
		   );
   localparam A = 3'd0; localparam B = 3'd1; localparam C = 3'd2;
   localparam D = 3'd3; localparam E = 3'd4;
   // assert(letterMask[0] == 1'b0); // Region A is never ignored
  always_comb
    case (currentLetter)
      A: casez (letterMask[4:1])
	   4'b???0: nextLetter = B;
	   4'b??01: nextLetter = C;
	   4'b?011: nextLetter = D;
	   4'b0111: nextLetter = E;
	   default nextLetter = A; // end
	 endcase
      B: casez (letterMask[4:2])
	   3'b??0: nextLetter = C;
	   3'b?01: nextLetter = D;
	   3'b011: nextLetter = E;
	   default nextLetter = B; // end
	 endcase
      C: casez (letterMask[4:3])
	   2'b?0: nextLetter = D;
	   2'b01: nextLetter = E;
	   default nextLetter = C; // end
	 endcase
      D: nextLetter = letterMask[4] ? D : E; // if ignoring E then end, else E
      E: nextLetter = E; // end
      default: nextLetter = 'X;
    endcase
endmodule   
