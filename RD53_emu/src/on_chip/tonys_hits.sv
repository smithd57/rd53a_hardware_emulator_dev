
// tclk: "clk80", hclk: "clk40", dclk: "clk160"; using hclk, fifo reads at dclk
module tonys_hits (
  input logic clk_i, clk2x_i, rst_i, // clk2x is synchronized to clk, but clk2x is twice the frequency
  input logic trigger_i, // start a trigger
  input logic update_output_i, // don't care
  input logic [31:0] trigger_info_i, // pass this through to the first 32-bit chunk sent out before the first hit data chunk
  // probably should get input holdDataFull from hitMaker2, otherwise trigger_data_o might be lost if fifo fills
  input logic [15:0] config_reg_i,
  output logic [63:0] trigger_data_o, // 2 chunks out
  output logic done_o // we're ready for another trigger
);
   // RD53A is 400x192 (wxh) pixels
   
   // a region = 4x1 pixels, a core = 8x8 pixels = 2x8 regions   
   // region IDs in a core are:
   //  0  1
   //  2  3
   //  4  5
   //  6  7
   //  8  9
   // 10 11
   // 12 13
   // 14 15
   // y is bigger than x by 2 bits because regions are 4 pixels wide by 1 tall
   // max pixel array size = 64x64 cores = 128x512 regions = 512x512 pixels

   logic [31:0] randDataClusterHit, randComp, randDataLineHit;
   logic 	newXYO, useLineType;
   // random data input to ClusterHit

   simplePRNG #(32) rngClusters (.clk(clk_i), .reset(rst_i), .load(1'b0), .step(1'b1),

				  .seed(32'hDEADBEEF), .dout(randDataClusterHit), .new_data());
   // [15:0] compare with config reg to change hit rate, [20:16] decide which hit generator to use (line or cluster)
   simplePRNG #(32) rngComp     (.clk(clk_i), .reset(rst_i), .load(1'b0), .step(1'b1),
				  .seed(32'hF00DFACE), .dout(randComp), .new_data());
   // random data input to LineHit
   simplePRNG #(32) rngLinegen  (.clk(clk_i), .reset(rst_i), .load(1'b0), .step(1'b1),
         			  .seed(32'hDABBAD00), .dout(randDataLineHit), .new_data());
   // useLineType=1: line hit, else cluster hit (lines are approx. 8% of hits in dataset, so use 3/32 = 9.4%)
   assign useLineType = randComp[20:16] < 5'd3;


   logic startClusterHit, doneClusterHit, startLineHit, doneLineHit, seenBloom, writeBloom, clearBloom;
   logic fifoRead, fifoWrite, fifoEmpty, clusterHitDelimiter, fifoRstBusy_wr, fifoRstBusy_rd;
   logic [31:0] clusterRegion, bloomRegion, fifoIn;
   logic [63:0] regionsLineHit;
   logic [65:0] fifoOut; // {2{1'hitDelimiter, 32'region}}
   logic [9:0] fifoUsage;
   localparam FIFO_GENERATE_THRESHOLD = 11'd800; // when fifoUsage < FIFO_GENERATE_THRESHOLD, generate a new clusterHit
   BloomFilter bloomFilter (.clk(clk2x_i), .rst(rst_i), .clear(clearBloom), .write(writeBloom), .region(bloomRegion), .seen(seenBloom));
   ClusterHit clusterHit (.clk(clk2x_i), .rst(rst_i), .start(startClusterHit), .randin(randDataClusterHit[27:0]), .region(clusterRegion), .done(doneClusterHit));
   LineHit lineHit (.clk(clk2x_i), .rst(rst_i), .start(startLineHit), .randin(randDataLineHit[27:0]), .regions(regionsLineHit), .done(doneLineHit));
   
   RegionDuplexingFIFO clusterHitBuffer (.rd_clk(clk_i), .rst(rst_i), .wr_clk(clk2x_i), .wr_rst_busy(fifoRstBusy_wr), .rd_rst_busy(fifoRstBusy_rd), .din({clusterHitDelimiter, fifoIn}), .wr_en(fifoWrite), .rd_en(fifoRead), .dout(fifoOut), .full(), .empty(fifoEmpty), .wr_data_count(fifoUsage));
   assign startLineHit = trigger_i & useLineType;


   localparam PAD_HIT = 16'h0000;
   logic [15:0] randomRegionAddr, padOddCluster_UNUSED;
   Cluster1x1 padOddCluster (.x(randDataClusterHit[17:9]), .y(randDataClusterHit[8:0]), .region({randomRegionAddr, padOddCluster_UNUSED}));
   logic hitReady, nhitReady, regionParity;
   enum {FIFO_WR_RST, DELIMIT, WAIT_FOR_FIFO, GENERATE, PAD_ODD} state, nextState;
   always_comb begin
      // Don't start a clusterHit or write to the Bloom Filter unless specified
      startClusterHit = 0;
      writeBloom = 0;
      clearBloom = 0;
      // Default to applying Bloom Filter to Cluster data
      bloomRegion = clusterRegion;
      // Default write nothing to the FIFO
      fifoWrite = 0;
      fifoIn = 'X;
      clusterHitDelimiter = 0;
      
      case (state)
	FIFO_WR_RST: begin
	   nextState = fifoRstBusy_wr ? FIFO_WR_RST : DELIMIT;
	end
	DELIMIT: begin // reset to this state
	   // Write a padding/delimiting region to be replaced by trigger info (triggerTag)
	   fifoIn = 32'h1864_0000;
	   clusterHitDelimiter = 1;
	   fifoWrite = 1;
		       
	   // "reset" the Bloom Filter by changing the timestamp
	   clearBloom = 1;

	   if (fifoUsage < FIFO_GENERATE_THRESHOLD) begin
	      startClusterHit = 1;
	      nextState = GENERATE;
	   end else begin
	      nextState = WAIT_FOR_FIFO;
	   end
	end
	WAIT_FOR_FIFO: begin
	   if (fifoUsage < FIFO_GENERATE_THRESHOLD) begin
	      startClusterHit = 1;
	      nextState = GENERATE;
	   end else begin
	      nextState = WAIT_FOR_FIFO;
	   end
	end
	GENERATE: begin
	   if (doneClusterHit) begin
	      if (regionParity) begin
		 nextState = PAD_ODD;
	      end else begin
		 nextState = DELIMIT;
	      end
	   end else begin
	      // clusterHit running, write output to FIFO if unique
	      fifoIn = clusterRegion;
	      fifoWrite = ~seenBloom;
	      
	      // add region address to non-uniques list
	      writeBloom = 1;
	      
	      nextState = GENERATE;
	   end
	end // case: GENERATE_CLUSTER
	PAD_ODD: begin
	   // Write a blank region to finish off the readout
	   bloomRegion = {randomRegionAddr, 16'hXXXX};
	   fifoIn = {randomRegionAddr, PAD_HIT};
	   if (seenBloom) begin
	      nextState = PAD_ODD;
	   end else begin
	      fifoWrite = 1;
	      nextState = DELIMIT;
	   end
	end
      endcase

      // (GENERATE or PAD_ODD) -> DELIMIT means we finished a hit
      if ((nextState == DELIMIT) & ((state == GENERATE)|(state == PAD_ODD))) begin
	 nhitReady = 1;
      end else begin
	 nhitReady = hitReady;
      end
   end
   always_ff @(posedge clk2x_i) begin
      if (rst_i | startClusterHit) regionParity <= 1;
      else                         regionParity <= ~regionParity;

      if (rst_i) begin
	 state <= FIFO_WR_RST;
	 regionParity <= 0;
      end else begin
	 state <= nextState;
	 hitReady <= nhitReady;
      end
   end
   // ClusterHit -> BloomFilter -> FIFO -> this module -> output
   // LineHit -> this module -> output


   logic [31:0] triggerInfoSaved;
   enum {IDLE, START, WORK_LINE, WORK_CLUSTER} ps, ns;
   always_comb begin
      // Default not done
      done_o = 0;
      // Default to not reading from FIFOs
      fifoRead = 0;
      // Don't infer latches
      trigger_data_o = 'X;

      case (ps)
	IDLE: begin
	   done_o = 1;
	   ns = (trigger_i & ~fifoRstBusy_rd) ? START : IDLE;
	end
	START: begin // Randomly choose to send a hit (if ready) or not
	   if ((randComp[15:0] >= config_reg_i) & (useLineType | (hitReady & ~useLineType))) begin
	      trigger_data_o[63:32] = triggerInfoSaved;
	      if (useLineType) begin // LineHit
		 trigger_data_o[31:0] = regionsLineHit[31:0];
		 ns = WORK_LINE;
	      end else begin // ClusterHit
	      if (fifoOut[32]) trigger_data_o[31:0] = fifoOut[64:33]; // bottom was delimiter, use top
	      else trigger_data_o[31:0] = fifoOut[31:0]; // top was delimter (this is expected), use bottom
		 fifoRead = 1;
		 ns = WORK_CLUSTER;
	      end
	   end else begin
	      // Return an empty frame
	      done_o = 1;
	      ns = trigger_i ? START : IDLE;
	   end
	end
	WORK_LINE: begin // Wait for line to finish
	   trigger_data_o = regionsLineHit;
	   done_o = doneLineHit;
	   ns = doneLineHit ? IDLE : WORK_LINE;
	end
	WORK_CLUSTER: begin // Send data from the fifo until the delimiter
	   if (fifoOut[65] | fifoOut[32] | fifoEmpty) begin // if either of the delimiters
	      if (fifoEmpty & ~(fifoOut[65] | fifoOut[32])) $display("%t WARNING: FIFO emptied. Hit readout corrupted.", $time);
	      // Finished reading out hit
	      done_o = 1;
	      ns = IDLE;
	   end else begin
	      // Continue reading out data from the FIFO
	      trigger_data_o = {fifoOut[64:33], fifoOut[31:0]};
	      fifoRead = 1;
	      ns = WORK_CLUSTER;
	   end
	end
      endcase
   end

   always_ff @(posedge clk_i) begin
      triggerInfoSaved <= trigger_i ? trigger_info_i : triggerInfoSaved;

      if (rst_i) ps <= IDLE;
      else ps <= ns;
   end
endmodule   
