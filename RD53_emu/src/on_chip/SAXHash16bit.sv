/* SAX hash function Algorithm: 
unsigned sax_hash(unsigned char *data, int len, unsigned seed) {
    unsigned h = seed;
    for (int i = 0; i < len; i++) {
        h ^= (h << 5) + (h >> 2) + data[i];
    }
    return h;
}
*/

// A combinational 2-byte SAX hash
module SAXHash16bit #(
		      OUTPUT_WIDTH=11
		      ) (
		     input logic [15:0] data,
		     input logic [OUTPUT_WIDTH-1:0] seed,
		     output logic [OUTPUT_WIDTH-1:0] hash
		     );
   // Left-shift by 5 adds 5 bits to keep track of
   logic [OUTPUT_WIDTH+5-1:0] first;

   assign first = seed ^ ((seed << 5) + (seed >> 2) + data[15:8]);
   assign hash = first ^ ((first << 5) + (first >> 2) + data[7:0]);
endmodule
