
// This file contains the various cluster generator modules used by the ClusterHit module //


//// UTILITY MODULES ////

// Given an array of valid/invalid choices, pick 2 indexes that are valid,
//  then output those indexes and output the valids with those indexes invalidated.
// Assumes at least 1 valid choice, undefined behavior otherwise.
// If only 1 valid choice, both indexes are that choice, and nextValids is all zeros.
module Pick2Valid #(N=9) (
			  input logic [N-1:0] 	       valids,
			  output logic [N-1:0] 	       nextValids,
			  output logic [$clog2(N)-1:0] indexA, indexB
			  );
   // Priority encoders
   int i, j;
   logic [N-1:0] validsMask;
   always_comb begin
      validsMask = '1;
      indexA = 'X;
      nextValids = valids;
      // Priority encoder A
      for (i = 0; i < N; i++) begin
	 if (valids[i]) begin
	    indexA = i;
	    nextValids[i] = 1'b0;
	    validsMask[i] = 1'b0;
	    break;
	 end
      end
      // Masked-input priority encoder B
      indexB = indexA; // send the same region twice if we don't find another
      for (j = 0; j < N; j++) begin
	 if (validsMask[j] & valids[j]) begin
	    indexB = j;
	    nextValids[j] = 1'b0;
	    break;
	 end
      end
   end
endmodule

// Takes in x,y coords of a pixel and produces the address of the region that contains that pixel
// WARNING: optimized for X_MAX=400, Y_MAX=192
module XY2RegionAddress #(X_MAX=400, Y_MAX=192, HIT=4'hC) (
         input logic [8:0] x, y,
	 output logic [15:0] addr,
	 output logic [1:0] subAddr
      );
   logic [8:0] xFix, yFix;
   // Optimization of xFix = x % X_MAX and yFix (assumes X_MAX=400,Y_MAX=192)
   always_comb begin
      xFix = (x >= X_MAX) ? (x - X_MAX) : x;
      if (y >= (Y_MAX+Y_MAX)) begin
	 yFix = y - Y_MAX - Y_MAX;
      end else if (y >= Y_MAX) begin
	 yFix = y - Y_MAX;
      end else begin
	 yFix = y;
      end
      // Check outputs iff inputs were valid
      assert((^x === 1'bX) | (xFix < X_MAX));
      assert((^y === 1'bX) | (yFix < Y_MAX));
   end
   assign addr = {xFix[8:3], yFix, xFix[2]};
   assign subAddr = xFix[1:0]; // pixel within the region addressed by addr
endmodule

// Takes in two regions (inA, inB) and either passes inA through to out (combined=0) or combines inA and inB into out (combined=1).
module RegionCombiner (
		       input logic [31:0] inA, inB,
		       output logic [31:0] out,
		       output logic combined
		       );
   // Copy address
   assign out[31:16] = inA[31:16];
   
   always_comb begin
      if (inA[31:16] == inB[31:16]) begin
	 combined = 1;
	 // Combine the 4 ToTs from each of the regions
	 // TODO: is adding the correct thing to do here?
	 out[15:0] = {
		 inA[15:12] | inB[15:12],
		 inA[11:8]  | inB[11:8],
		 inA[7:4]   | inB[7:4],
		 inA[3:0]   | inB[3:0]
		       };
      end else begin
	 combined = 0;
	 out[15:0] = inA[15:0];
      end
   end
endmodule

//// CLUSTER GENERATORS ////

// A single pixel
module Cluster1x1 #(X_MAX=400, Y_MAX=192, HIT=4'hC) (
         input logic [8:0] x, y,
	 output logic [31:0] region
      );
   logic [1:0] subAddr;
   XY2RegionAddress #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) coords (.x, .y, .addr(region[31:16]), .subAddr);
   
   always_comb begin
      // Default ToT values to 0
      region[15:0] = 16'd0;
      case (subAddr)
	2'd0: region[15:12] = HIT;
	2'd1: region[11:8] = HIT;
	2'd2: region[7:4] = HIT;
	2'd3: region[3:0] = HIT;
      endcase
   end
endmodule

// [1, 1] in any rotation
module Cluster2x1 #(X_MAX=400, Y_MAX=192, HIT=4'hC) (
         input logic [8:0] x, y,
         input logic [1:0] orientation,
         output logic [31:0] regionA, regionB,
         output logic ignoreB
      );
   logic [8:0] xB, yB;

   logic [31:0] rA;
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterA (.x, .y, .region(rA));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterB (.x(xB), .y(yB), .region(regionB));
   RegionCombiner regionComb (.inA(rA), .inB(regionB), .out(regionA), .combined(ignoreB));
   
   always_comb begin
      case (orientation)
	2'd0: {xB, yB} = {x+9'd1, y};
	2'd1: {xB, yB} = {x-9'd1, y};
	2'd2: {xB, yB} = {x, y+9'd1};
	2'd3: {xB, yB} = {x, y-9'd1};
      endcase
   end
endmodule

// [1, 1, 1] in any orientation
module Cluster3x1 #(X_MAX=400, Y_MAX=192, HIT=4'hC) (
         input logic [8:0] x, y,
         input logic [1:0] orientation,
         output logic [31:0] regionA, regionB, regionC,
         output logic ignoreB, ignoreC
      );
   logic [8:0] xB, xC, yB, yC;

   logic [31:0] rA, rB, rC, rAC, rBC;
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterA (.x, .y, .region(rA));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterB (.x(xB), .y(yB), .region(rB));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterC (.x(xC), .y(yC), .region(regionC));
   // if anything gets combined, C will be absorbed into A or B and then A & B may or may not also merge
   logic combAC, combBC;
   RegionCombiner regionCombAC (.inA(rA), .inB(regionC), .out(rAC), .combined(combAC));
   RegionCombiner regionCombBC (.inA(rB), .inB(regionC), .out(regionB), .combined(combBC));
   // if C was absorbed by A then ignore the combination of B & C
   RegionCombiner regionCombAB (.inA(rAC), .inB(combAC ? rB : regionB), .out(regionA), .combined(ignoreB));
   assign ignoreC = combAC | combBC; // if it was combined into either, ignore it
   
   always_comb begin
      // Default to unchanged
      {xB, xC, yB, yC} = {x, x, y, y};
      
      case (orientation) // [A, C, B], because C will be ignored more often than B
	2'd0: {xC, xB} = {x+9'd1, x+9'd2};
	2'd1: {xC, xB} = {x-9'd1, x-9'd2};
	2'd2: {yC, yB} = {y+9'd1, y+9'd2};
	2'd3: {yC, yB} = {y-9'd1, y-9'd2};
      endcase
   end
endmodule

// Any rotation of
// 0 1
// 1 1 <- (bottom right 1, I'm naming this the "primary" pixel)
// x,y are the location of the primary pixel.
// Note: we don't have to send the zero if it's in its own region, hence only 3 regions.
module Cluster2x2_1z #(X_MAX=400, Y_MAX=192, HIT=4'hC) (
         input logic [8:0] x, y,
         input logic [1:0] orientation,
         output logic [31:0] regionA, regionB, regionC,
         output logic ignoreB, ignoreC
      );
   logic [8:0] xC, yB;

   // Region B is the one above or below region A, so it can never be combined
   assign ignoreB = 1'b0;

   logic [31:0] rA;
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterA (.x, .y, .region(rA));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterB (.x(x), .y(yB), .region(regionB));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterC (.x(xC), .y(y), .region(regionC));
   RegionCombiner regionCombAC (.inA(rA), .inB(regionC), .out(regionA), .combined(ignoreC));
   
   always_comb begin
      // Pixel C is chosen as whichever is to the side of the primary.
      // Pixel B is chosen as whichever is above or below the primary.
      
      // Default to unchanged
      {xC, yB} = {x, y};
      case (orientation)
	2'd0: {xC, yB} = {x-9'd1, y+9'd1}; // in comment
	2'd1: {xC, yB} = {x+9'd1, y+9'd1}; // rotated 90 deg. clockwise
	2'd2: {xC, yB} = {x+9'd1, y-9'd1}; // rotated 180 deg. clkwise
	2'd3: {xC, yB} = {x-9'd1, y-9'd1}; // rotated 270 deg. cw
      endcase
   end
endmodule

// Any rotation or mirror of 
// 0 0 1
// 1 1 1 <- (bottom right 1, I'm naming this the "primary" pixel)
// x,y are the location of the primary pixel.
module ClusterL # (X_MAX=400, Y_MAX=192, HIT=4'hC) (
	input logic [8:0] x, y,
	input logic [2:0] orientation,
	output logic [31:0] regionA, regionB, regionC, regionD,
	output logic ignoreB, ignoreC, ignoreD
);
	       logic [8:0] xB, xC, xD, yB, yC, yD;
   
   logic [31:0] 	   rA, rA2, rC;
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterA (.x, .y, .region(rA));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterB (.x(xB), .y(yB), .region(regionB));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterC (.x(xC), .y(yC), .region(rC));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterD (.x(xD), .y(yD), .region(regionD));
   RegionCombiner regionComb1 (.inA(rC), .inB(regionB), .out(regionC), .combined(ignoreB));
   RegionCombiner regionComb2 (.inA(rA), .inB(regionC), .out(rA2), .combined(ignoreC));
   RegionCombiner regionComb3 (.inA(rA2), .inB(regionD), .out(regionA), .combined(ignoreD));
   
   always_comb begin
      // Pixel B is chosen as two away from primary.
      // Pixel C is chosen between Pixel B and primary.
      // Pixel D is chosen as other pixel next to primary.
      case(orientation)
	3'd0: {xB, yB, xC, yC, xD, yD} = {x-9'd2, y, x-9'd1, y, x, y+9'd1}; // in comment
	3'd1: {xB, yB, xC, yC, xD, yD} = {x, y+9'd2, x, y+9'd1, x+9'd1, y}; // rotated 90 deg. clockwise
	3'd2: {xB, yB, xC, yC, xD, yD} = {x+9'd2, y, x+9'd1, y, x, y-9'd1}; // rotated 180 deg. cw
	3'd3: {xB, yB, xC, yC, xD, yD} = {x, y-9'd2, x, y-9'd1, x-9'd1, y}; // rotated 270 deg. cw
	3'd4: {xB, yB, xC, yC, xD, yD} = {x+9'd2, y, x+9'd1, y, x, y+9'd1}; // mirrored
	3'd5: {xB, yB, xC, yC, xD, yD} = {x, y-9'd2, x, y-9'd1, x+9'd1, y}; // mirrored and rotated 90 deg. cw
	3'd6: {xB, yB, xC, yC, xD, yD} = {x-9'd2, y, x-9'd1, y, x, y-9'd1}; // mirrored and rotated 180 deg. cw
	3'd7: {xB, yB, xC, yC, xD, yD} = {x, y+9'd2, x, y+9'd1, x-9'd1, y}; // mirrored and rotated 270 deg. cw
      endcase
   end 
endmodule

// Any rotation of 
// 0 1
// 1 1 <- (I'm naming this the "primary" pixel)
// 0 1
// x,y are the location of the primary pixel.
module ClusterT # (X_MAX=400, Y_MAX=192, HIT=4'hC) (
	input logic [8:0] x, y,
	input logic [1:0] orientation,
	output logic [31:0] regionA, regionB, regionC, regionD,
	output logic ignoreB, ignoreC, ignoreD
);
	logic [8:0] xB, xC, xD, yB, yC, yD;
	
	logic [31:0] rA, rA2, rA3;
	Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterA (.x, .y, .region(rA));
    Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterB (.x(xB), .y(yB), .region(regionB));
    Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterC (.x(xC), .y(yC), .region(regionC));
	Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterD (.x(xD), .y(yD), .region(regionD));
	RegionCombiner regionComb1 (.inA(rA), .inB(regionB), .out(rA2), .combined(ignoreB));
	RegionCombiner regionComb2 (.inA(rA2), .inB(regionC), .out(rA3), .combined(ignoreC));
	RegionCombiner regionComb3 (.inA(rA3), .inB(regionD), .out(regionA), .combined(ignoreD));
	
	always_comb begin
	// Pixel B is chosen as above primary (in comment)
	// Pixel C is chosen as below primary (in comment)
	// Pixel D is chosen as left of primary (in comment)
		case(orientation)
			2'd0: {xB, yB, xC, yC, xD, yD} = {x, y+9'd1, x, y-9'd1, x-9'd1, y}; // in comment
			2'd1: {xB, yB, xC, yC, xD, yD} = {x+9'd1, y, x-9'd1, y, x, y+9'd1}; // rotated 90 deg. clockwise
			2'd2: {xB, yB, xC, yC, xD, yD} = {x, y-9'd1, x, y+9'd1, x+9'd1, y}; // rotated 180 deg. cw
			2'd3: {xB, yB, xC, yC, xD, yD} = {x-9'd1, y, x+9'd1, y, x, y-9'd1}; // rotated 270 deg. cw
		endcase
	end 
endmodule	

// Any rotation or mirror of 
// 0 1
// 1 1 <- (I'm naming this the "primary" pixel)
// 1 0
// x,y are the location of the primary pixel.
module ClusterZ # (X_MAX=400, Y_MAX=192, HIT=4'hC) (
	input logic [8:0] x, y,
	input logic [1:0] orientation,
	output logic [31:0] regionA, regionB, regionC, regionD,
	output logic ignoreB, ignoreC, ignoreD
);
	logic [8:0] xB, xC, xD, yB, yC, yD;
	
	logic [31:0] rA, rA2, rC;
	Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterA (.x, .y, .region(rA));
    Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterB (.x(xB), .y(yB), .region(regionB));
    Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterC (.x(xC), .y(yC), .region(rC));
	Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterD (.x(xD), .y(yD), .region(regionD));
	RegionCombiner regionComb1 (.inA(rC), .inB(regionD), .out(regionC), .combined(ignoreD));
	RegionCombiner regionComb2 (.inA(rA), .inB(regionB), .out(rA2), .combined(ignoreB));
	RegionCombiner regionComb3 (.inA(rA2), .inB(regionC), .out(regionA), .combined(ignoreC));
	
	always_comb begin
	// Pixel B is chosen as above primary (in comment)
	// Pixel C is chosen as left of primary (in comment)
	// Pixel D is chosen as diagonal of primary (in comment)
		case(orientation)
			2'd0: {xB, yB, xC, yC, xD, yD} = {x, y+9'd1, x-9'd1, y, x-9'd1, y-9'd1}; // in comment
			2'd1: {xB, yB, xC, yC, xD, yD} = {x+9'd1, y, x, y+9'd1, x-9'd1, y+9'd1}; // rotated 90 deg. clockwise
			2'd2: {xB, yB, xC, yC, xD, yD} = {x, y+9'd1, x+9'd1, y, x+9'd1, y-9'd1}; // mirrored
			2'd3: {xB, yB, xC, yC, xD, yD} = {x+9'd1, y, x, y-9'd1, x-9'd1, y-9'd1}; // mirrored and rotated 90 deg. cw
		endcase
	end 
endmodule	

// Any rotation or mirror of 
// 1 1 
// 1 1 
// 1 0 <- (bottom left 1, I'm naming this the "primary" pixel)
// x,y are the location of the primary pixel.
module ClusterP # (X_MAX=400, Y_MAX=192, HIT=4'hC) (
	input logic [8:0] x, y,
	input logic [2:0] orientation,
	output logic [31:0] regionA, regionB, regionC, regionD, regionE,
	output logic ignoreB, ignoreC, ignoreD, ignoreE
     );
   logic [8:0] xB, xC, xD, xE, yB, yC, yD, yE;
   
   logic [31:0] rA, rB, rC, rD, rE;
   logic ignoreD1, ignoreD2;
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterA (.x, .y, .region(rA));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterB (.x(xB), .y(yB), .region(rB));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterC (.x(xC), .y(yC), .region(rC));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterD (.x(xD), .y(yD), .region(rD));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterE (.x(xE), .y(yE), .region(rE));
   RegionCombiner regionComb1 (.inA(rE), .inB(rD), .out(regionE), .combined(ignoreD1));
   RegionCombiner regionComb2 (.inA(rC), .inB(regionE), .out(regionC), .combined(ignoreE));
   RegionCombiner regionComb3 (.inA(rB), .inB(regionC), .out(regionB), .combined(ignoreC));
   RegionCombiner regionComb4 (.inA(rD), .inB(regionB), .out(regionD), .combined(ignoreB));
   RegionCombiner regionComb5 (.inA(rA), .inB(regionD), .out(regionA), .combined(ignoreD2));
   
   assign ignoreD = ignoreD1 | ignoreD2;
   
   always_comb begin
      // B C 
      // D E
      // 1 0 <- bottom left is primary
      case(orientation)
	3'd0: {xB, yB, xC, yC, xD, yD, xE, yE} = {x, y+9'd2, x+9'd1, y+9'd2, x, y+9'd1, x+9'd1, y+9'd1}; // in comment
	3'd1: {xB, yB, xC, yC, xD, yD, xE, yE} = {x+9'd2, y, x+9'd2, y-9'd1, x+9'd1, y, x+9'd1, y-9'd1}; // rotated 90 deg. clockwise
	3'd2: {xB, yB, xC, yC, xD, yD, xE, yE} = {x, y-9'd2, x-9'd1, y-9'd2, x, y-9'd1, x-9'd1, y-9'd1}; // rotated 180 deg. cw
	3'd3: {xB, yB, xC, yC, xD, yD, xE, yE} = {x-9'd2, y, x-9'd2, y+9'd1, x-9'd1, y, x-9'd1, y+9'd1}; // rotated 270 deg. cw
	3'd4: {xB, yB, xC, yC, xD, yD, xE, yE} = {x, y+9'd2, x-9'd1, y+9'd2, x, y+9'd1, x-9'd1, y+9'd1}; // mirrored
	3'd5: {xB, yB, xC, yC, xD, yD, xE, yE} = {x+9'd2, y, x+9'd2, y+9'd1, x+9'd1, y, x+9'd1, y+9'd1}; // mirrored and rotated 90 deg. cw
	3'd6: {xB, yB, xC, yC, xD, yD, xE, yE} = {x, y-9'd2, x+9'd1, y-9'd2, x, y-9'd1, x+9'd1, y-9'd1}; // mirrored and rotated 180 deg. cw
	3'd7: {xB, yB, xC, yC, xD, yD, xE, yE} = {x-9'd2, y, x-9'd2, y-9'd1, x-9'd1, y, x-9'd1, y-9'd1}; // mirrored and rotated 270 deg. cw
      endcase
   end 
endmodule

// 1 1
// 1 1 <- (I'm naming this the "primary" pixel)
// x,y are the location of the primary pixel.
module ClusterO # (X_MAX=400, Y_MAX=192, HIT=4'hC) (
	input logic [8:0] x, y,
	input logic [1:0] orientation,
	output logic [31:0] regionA, regionB, regionC, regionD,
	output logic ignoreB, ignoreC, ignoreD
);
   logic [8:0] xB, xC, xD, yB, yC, yD;
   
   logic [31:0] rA, rA2, rB, rD;
   logic 	ignoreC1, ignoreC2;
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterA (.x, .y, .region(rA));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterB (.x(xB), .y(yB), .region(rB));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterC (.x(xC), .y(yC), .region(regionC));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterD (.x(xD), .y(yD), .region(rD));
   RegionCombiner regionComb1 (.inA(rB), .inB(regionC), .out(regionB), .combined(ignoreC1));
   RegionCombiner regionComb2 (.inA(rD), .inB(regionB), .out(regionD), .combined(ignoreB));
   RegionCombiner regionComb3 (.inA(rA), .inB(regionD), .out(rA2), .combined(ignoreD));
   RegionCombiner regionComb4 (.inA(rA2), .inB(regionC), .out(regionA), .combined(ignoreC2));
   
   assign ignoreC = ignoreC1 | ignoreC2;
   
   always_comb begin
      // B C 
      // D 1 <- primary
      case(orientation)
	2'd0: {xB, yB, xC, yC, xD, yD} = {x-9'd1, y+9'd1, x, y+9'd1, x-9'd1, y}; // in comment
	2'd1: {xB, yB, xC, yC, xD, yD} = {x+9'd1, y+9'd1, x+9'd1, y, x, y+9'd1}; // rotated 90 deg. clockwise
	2'd2: {xB, yB, xC, yC, xD, yD} = {x+9'd1, y-9'd1, x, y-9'd1, x+9'd1, y}; // rotated 180 deg. cw
	2'd3: {xB, yB, xC, yC, xD, yD} = {x-9'd1, y-9'd1, x-9'd1, y, x, y-9'd1}; // rotated 270 deg. cw
      endcase
   end 
endmodule

// Any rotation or mirror of 
// 0 1 1
// 1 1 0 <- (middle 1, I'm naming this the "primary" pixel)
// 0 1 0 
// x,y are the location of the primary pixel.
module ClusterF # (X_MAX=400, Y_MAX=192, HIT=4'hC) (
	input logic [8:0] x, y,
	input logic [2:0] orientation,
	output logic [31:0] regionA, regionB, regionC, regionD, regionE,
	output logic ignoreB, ignoreC, ignoreD, ignoreE
);
   logic [8:0] xB, xC, xD, xE, yB, yC, yD, yE;
   
   logic [31:0] 	   rA, rA2, rA3, rB;
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterA (.x, .y, .region(rA));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterB (.x(xB), .y(yB), .region(rB));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterC (.x(xC), .y(yC), .region(regionC));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterD (.x(xD), .y(yD), .region(regionD));
   Cluster1x1 #(.X_MAX(X_MAX), .Y_MAX(Y_MAX)) clusterE (.x(xE), .y(yE), .region(regionE));
   RegionCombiner regionComb1 (.inA(rB), .inB(regionC), .out(regionB), .combined(ignoreC));
   RegionCombiner regionComb2 (.inA(rA), .inB(regionB), .out(rA2), .combined(ignoreB));
   RegionCombiner regionComb3 (.inA(rA2), .inB(regionD), .out(rA3), .combined(ignoreD));
   RegionCombiner regionComb4 (.inA(rA3), .inB(regionE), .out(regionA), .combined(ignoreE));
   
   always_comb begin
      // 0 B C 
      // D 1 0 <- middle is primary
      // 0 E 0
      case(orientation)
	3'd0: {xB, yB, xC, yC, xD, yD, xE, yE} = {x, y+9'd1, x+9'd1, y+9'd1, x-9'd1, y, x, y-9'd1}; // in comment
	3'd1: {xB, yB, xC, yC, xD, yD, xE, yE} = {x+9'd1, y, x+9'd1, y-9'd1, x, y+9'd1, x-9'd1, y}; // rotated 90 deg. clockwise
	3'd2: {xB, yB, xC, yC, xD, yD, xE, yE} = {x, y-9'd1, x-9'd1, y-9'd1, x+9'd1, y, x, y+9'd1}; // rotated 180 deg. cw
	3'd3: {xB, yB, xC, yC, xD, yD, xE, yE} = {x-9'd1, y, x-9'd1, y+9'd1, x, y-9'd1, x+9'd1, y}; // rotated 270 deg. cw
	3'd4: {xB, yB, xC, yC, xD, yD, xE, yE} = {x, y+9'd1, x-9'd1, y+9'd1, x+9'd1, y, x, y-9'd1}; // mirrored
	3'd5: {xB, yB, xC, yC, xD, yD, xE, yE} = {x+9'd1, y, x+9'd1, y+9'd1, x, y-9'd1, x-9'd1, y}; // mirrored and rotated 90 deg. cw
	3'd6: {xB, yB, xC, yC, xD, yD, xE, yE} = {x, y-9'd1, x+9'd1, y-9'd1, x-9'd1, y, x, y+9'd1}; // mirrored and rotated 180 deg. cw
	3'd7: {xB, yB, xC, yC, xD, yD, xE, yE} = {x-9'd1, y, x-9'd1, y-9'd1, x, y+9'd1, x+9'd1, y}; // mirrored and rotated 270 deg. cw
      endcase
   end 
endmodule
