module a_couple_hits (
  input logic clk_i, rst_i,
  input logic trigger_i,
  input logic update_output_i,
  input logic [31:0] trigger_info_i,
  output logic [63:0] trigger_data_o,
  output logic done_o
);
  //logic for which pixel to start each sub cycle on and which data points are active for the whole cycle
  logic [3:0] active_point, start_point;
  logic start, done;
  logic [63:0] trigger_data;
  logic [11:0] counter_full;
  
  logic [3:0] ADC_random, region, region1;
  logic [5:0] col_rand, row_rand, col_rand1, row_rand1;
  
  /*LFSRx4 ADC_rand (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .start(4'hf),
    .rnd_o(ADC_random) 
  );*/
  assign ADC_random = 4'ha;
  
  LFSRx4 region_rand (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .start(4'hf),//4'h4),
    .rnd_o(region) 
  );
  
  LFSRx6_0 rand_col (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .start(6'h13),//6'h2f),
    .rnd_o (col_rand)
  );
  
  LFSRx6_1 rand_row (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .start(5'h07),//5'h1f),
    .rnd_o (row_rand)
  );
  
  LFSRx4 region_rand1 (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .start(4'h8),
    .rnd_o(region1) 
  );
  
  LFSRx6_0 rand_col1(
    .clk_i(clk_i),
    .rst_i(rst_i),
    .start(6'h08),
    .rnd_o (col_rand1)
  );
  
  LFSRx6_1 rand_row1 (
    .clk_i(clk_i),
    .rst_i(rst_i),
    .start(5'h1a),//5'h08),
    .rnd_o (row_rand1)
  );
  
  logic [15:0] address, address1, data;
  
  assign address[15:10] = col_rand;
  assign address[9:4] = row_rand;
  assign address[3:0] = region;  
  
  assign address1[15:10] = col_rand1;
  assign address1[9:4] = row_rand1;
  assign address1[3:0] = region1;

  assign data[15:12] = ADC_random;
  assign data[11:8] = ADC_random;
  assign data[7:4] = ADC_random;
  assign data[3:0] = ADC_random;
  
  //response logic for a trigger
  always @(posedge clk_i) begin
    if(rst_i) begin
      //reset behavior
      done <= 1;
      start <= 0;
      
      trigger_data <= 0;
      
      counter_full <= 0;
    end else if(trigger_i == 1 & start == 0 & done == 1) begin
      //when a new trigger comes in send out a header and the first piece of data
      done <= 0;
      start <= 1;
      
      trigger_data[63:32] <= trigger_info_i;
      trigger_data[31:16] <= address;
      trigger_data[15:0] <= data;
      
      counter_full <= counter_full + 1;
    end else if(counter_full < 3 & start == 1) begin
      //general behavior
      done <= 0;
      start <= 1;
      
      trigger_data[63:48] <= address1;
      trigger_data[47:32] <= data;
      trigger_data[31:16] <= address;
      trigger_data[15:0] <= data;
      
      counter_full <= counter_full + 1;
    end else if (counter_full == 3 & start == 1) begin
      done <= 0;
      start <= 0;
      
      trigger_data[63:48] <= address1;
      trigger_data[47:32] <= data;
      trigger_data[31:16] <= address;
      trigger_data[15:0] <= data;
      
      counter_full <= counter_full;
    end
    else begin
      //close off the outputs
      done <= 1;
      start <= 0;
      
      trigger_data <= 0;
      
      counter_full <= 0;
      //counter_sub <= counter_sub + 1;
    end
  end
  
  assign done_o = done;
  assign trigger_data_o = trigger_data;
  
endmodule

module a_couple_hits_testbench();
  //the logic 
  logic clk_i, rst_i;
  logic trigger_i;
  logic update_output_i;
  logic [31:0] trigger_info_i;
  logic [63:0] trigger_data_o;
  logic done_o;


  
  a_couple_hits dut (
  .clk_i, 
  .rst_i,
  .trigger_i,
  .update_output_i,
  .trigger_info_i,
  .trigger_data_o,
  .done_o);
  
  
  //set up the clock
  parameter ClockDelayT = 125;
  initial begin
      clk_i <= 0;
      forever #(ClockDelayT/2) clk_i <= ~clk_i;
  end
  
  initial begin
    trigger_i <= 0; 
    update_output_i <= 0;  
    trigger_info_i <= 32'hFFFFFFFF; 
    rst_i <= 1'b1;	@(posedge clk_i);
    rst_i <= 1'b0; @(posedge clk_i);
    
    repeat(64) begin
    trigger_i <= 1'b1;@(posedge clk_i);
    trigger_i <= 1'b0;@(posedge clk_i);
    
    repeat (650)  begin @(posedge clk_i); end;
    end
    
    
    $stop(); // end the simulation
end
	
endmodule
    
  
  
  
  