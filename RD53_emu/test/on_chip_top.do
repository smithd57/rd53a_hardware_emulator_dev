vlib work

# Testing modules
vlog -work work ../src/on_chip/on_chip_top.sv
vlog -work work on_chip_top_tb.sv

# RD53A Modules
vlog -work work ../src/on_chip/SRL.v
vlog -work work ../src/on_chip/shift_align.v
vlog -work work ../src/on_chip/clock_picker.v 
vlog -work work ../src/on_chip/ttc_top.v 
vlog -work work ../src/on_chip/trigger_counter.v
vlog -work work ../src/on_chip/command_process.v
vlog -work work ../src/on_chip/hitMaker.v
vlog -work work ../src/on_chip/updatedPRNG.v
vlog -work work ../src/on_chip/command_out.v
vlog -work work ../src/on_chip/chip_output.sv
vlog -work work ../src/on_chip/aurora_tx_top.sv
vlog -work work ../src/on_chip/aurora_tx_four_lane.sv
vlog -work work ../src/on_chip/scrambler.v
vlog -work work ../src/on_chip/gearbox_66_to_32.sv
vlog -work work ../src/on_chip/frame_buffer.sv
vlog -work work ../src/on_chip/frame_incrementer.sv
vlog -work work ../src/on_chip/frame_buffer_four_lane.sv

# Aurora Rx Modules
vlog -work work ../src/aurora/descrambler.v
vlog -work work ../src/aurora/gearbox_32_to_66.v
vlog -work work ../src/aurora/block_sync.v
vlog -work work ../src/aurora/bitslip_fsm.sv
vlog -work work ../src/aurora/channel_bond.sv
vcom -work work ../src/aurora/fifo_fwft_funcsim.vhdl
vlog -work work ../src/aurora/delay_controller_wrap.v
vlog -work work ../src/aurora/serdes_1_to_468_idelay_ddr.v
vlog -work work ../src/aurora/aurora_rx_top_xapp.sv
vlog -work work ../src/aurora/aurora_rx_top.sv

# Aurora Rx ISERDES IP Core
vcom -work work ../src/aurora/cmd_iserdes_funcsim.vhdl


# Xilinx IP Cores
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/clk_wiz_2/clk_wiz_2_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/fifo_generator_0/fifo_generator_0_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/fifo_generator_1/fifo_generator_1_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/fifo_generator_2/fifo_generator_2_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/triggerFifo/triggerFifo_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/hitDataFIFO/hitDataFIFO_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/cmd_oserdes/cmd_oserdes_sim_netlist.vhdl
# vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/ila_1/ila_1_sim_netlist.vhdl
# vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/vio_0/vio_0_sim_netlist.vhdl
 
vsim -t 1ps -novopt on_chip_top_tb -L unisim -L secureip -L unifast -L unimacro

view signals
view wave

do wave_on_chip_top.do

#run 12us
run 700us
