vlib work

vlog -work work ../src/on_chip/updatedPRNG.v
vlog -work work ../src/on_chip/clusters.sv
vlog -work work ./clusters_tb.sv

vsim -t 1ps -novopt work.clusters_tb

view signals
view wave

do wave_clusters.do

run 10 ms
