onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix binary -radixshowbase 0 /HLine_tb/dut/clk
add wave -noupdate -radix binary -radixshowbase 0 /HLine_tb/dut/rst
add wave -noupdate /HLine_tb/dut/region
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/y
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/curY
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/length
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/limitedLength
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/x
add wave -noupdate -radix binary -radixshowbase 0 /HLine_tb/dut/done
add wave -noupdate -radix binary -radixshowbase 0 /HLine_tb/dut/finishedLine
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/curX
add wave -noupdate -radix binary -radixshowbase 0 /HLine_tb/dut/tot
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/stop
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/nextStop
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/diff
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/subAddr
add wave -noupdate -radix binary -radixshowbase 0 /HLine_tb/dut/totTmp
add wave -noupdate -radix binary -radixshowbase 0 /HLine_tb/dut/regionParity
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/X_MAX
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/Y_MAX
add wave -noupdate -radix hexadecimal -radixshowbase 0 /HLine_tb/dut/HIT
add wave -noupdate -radix hexadecimal -radixshowbase 0 /HLine_tb/dut/PAD_HIT
add wave -noupdate -divider spacing
add wave -noupdate -divider spacing
add wave -noupdate -divider {regAddr(x, y)}
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/coords/X_MAX
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/coords/Y_MAX
add wave -noupdate -radixshowbase 0 /HLine_tb/dut/coords/HIT
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/coords/x
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/coords/y
add wave -noupdate -radixshowbase 0 /HLine_tb/dut/coords/addr
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/coords/subAddr
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/coords/xFix
add wave -noupdate -radix unsigned -radixshowbase 0 /HLine_tb/dut/coords/yFix
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {117563 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {463314 ps}
