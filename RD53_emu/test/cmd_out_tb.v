// testbench file for command_out

`timescale 1ps/1ps 

module cmd_out_tb;

reg clk160, clk80;
reg rst;
wire next_hit;
reg [63:0] hitin;
reg wr_cmd;
reg [15:0] datain;
reg wr_adx;
reg [8:0] adxin;
reg present_frame; 
reg hitData_empty; 
wire cmd_full, adx_full;
wire [63:0] data_out;
wire data_out_valid;
wire service_frame;

parameter halfclk80 = 6250;
parameter halfclk160 = 3125;

command_out uut(
    .clk80(clk80),
    .clk160(clk160),
    .rst(rst),
    .next_hit(next_hit),
    .hitin(hitin),
	.hitData_empty(/*hitData_empty*/ 1'b1),
    .wr_cmd(wr_cmd),
    .cmdin(datain),
    .wr_adx(wr_adx),
    .adxin(adxin),
	.present_frame(present_frame),
    .cmd_full(cmd_full),
    .adx_full(adx_full),
    .data_out(data_out),
    .data_out_valid(data_out_valid),
    .service_frame(service_frame)
	
);

//always #halfclk80
//    clk80 = ~clk80;
  
always #halfclk160
    clk160 = ~clk160;
	
always @(posedge clk160) begin
	clk80 = ~clk80; 
end

initial begin
    clk160 <= 1'b0;
    clk80 <= 1'b0;
    wr_cmd <= 1'b0;
    wr_adx <= 1'b0;
    rst <= 1'b0;
    datain <= 16'b0;
    adxin <= 9'b0;
	present_frame <= 1'b1; 
end

initial begin 
	forever begin 
		if(($urandom()%2)) begin 
			@(posedge clk80);
			hitData_empty <= 1'b0;
		end else begin
			@(posedge clk80);
			hitData_empty <= 1'b1;
		end
	end 
end 

//Stimulus
initial begin
    @(posedge clk80);
    rst <= 1'b1;
    repeat (8) @(posedge clk80);
    rst <= 1'b0;
    repeat (4) @(posedge clk80);
    wr_cmd <= 1'b1;
    wr_adx <= 1'b1;
    hitin <= 64'hF0F0F0F0F0F0F0F0;
    datain <= 16'b1010101010101010;
    adxin <= 9'b111000111;
    @(posedge clk80);
    @(posedge clk80);
    wr_cmd <= 1'b0;
    wr_adx <= 1'b0;
    @(posedge clk80);
    @(posedge clk80);
    @(posedge clk80);
    repeat (38) @(posedge clk80);  // allow word to pass
    hitin <= 64'h00FF00FF00FF00FF;
    @(posedge clk80);
    @(posedge clk80);
    wr_cmd <= 1'b1;
    wr_adx <= 1'b1;
    datain <= 16'b0011001100110011;
    adxin <= 9'b110000011;
    @(posedge clk80);
    @(posedge clk80);
    adxin <= 9'b001111100;
    @(posedge clk80);
    @(posedge clk80);
    wr_adx <= 1'b0;
    wr_cmd <= 1'b0;
    @(posedge clk80);
    @(posedge clk80);
    @(posedge clk80);
    @(posedge clk80);
    repeat (40) @(posedge clk80);  // allow first word to pass
    repeat (48) @(posedge clk80);  // allow second word to pass
    repeat (38) @(posedge clk80);  // next word comes just before service frame 
    wr_cmd <= 1'b1;
    wr_adx <= 1'b1;
    datain <= 16'b0000111100001111;
    adxin <= 9'b100111001;
    @(posedge clk80);
    @(posedge clk80);
    wr_adx <= 1'b0;
    wr_cmd <= 1'b0;
    @(posedge clk80);
    @(posedge clk80);
    repeat (60) @(posedge clk80);  // service frame w/o data
    $stop;
end
     
endmodule
