
`timescale 1ns / 1ps

module BloomFilter_tb ();
   logic clk, rst, clear, write, seen;
   logic [31:0] region;

   BloomFilter dut (.*);

   parameter FILE_NAME = "testData_bloomFilter.txt";
   parameter FILE_LENGTH = 848604;

   // {4'metaData, 32'region}
   logic [35:0] testData [FILE_LENGTH-1:0];
   
   //set up the clock
   parameter ClockDelayT = 125;
   initial begin
      clk <= 0;
      forever #(ClockDelayT/2) clk <= ~clk;
   end

   int i, falsePositives, expectedFalsePositives;
   logic [3:0] meta;
   initial begin
      $readmemh(FILE_NAME, testData);
      {write, clear} = '0;
      rst = 1;
      // Initialize the Bloom Filter memory to zero to make it match the tests
      //for (i = 0; i < dut.DEPTH; i++) dut.mem[i] = '0; // same as dut.mem = {default: '0};
      @(negedge clk);
      rst = 0;

      falsePositives = 0;
      for (i = 0; i < FILE_LENGTH; i++) begin
	 {meta, region} = testData[i];
	 if (meta == 4'hF) begin
	    clear = 1; // clear the filter so we can do a new set
	 end else begin
	    #10;
	    write = 1;
	    assert(seen == meta[1]); // unexpected false positive
	    if (meta[0] != meta[1]) begin // expected false positive
	       expectedFalsePositives++;
	    end
	    if (seen != meta[0]) begin
	       $display("%t: false positive", $time);
	       falsePositives++;
	    end
	    @(negedge clk);
	    clear = 0;
	 end
      end

      $display("Saw %d false positives total.", falsePositives);

      $stop;
   end
endmodule
