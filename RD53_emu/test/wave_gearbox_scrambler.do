onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group Top /gearbox_scrambler_tb/clk
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /gearbox_scrambler_tb/scr/data_in
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /gearbox_scrambler_tb/scr/data_out
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /gearbox_scrambler_tb/scr/poly
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /gearbox_scrambler_tb/scr/scrambler
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /gearbox_scrambler_tb/scr/enable
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold /gearbox_scrambler_tb/scr/rst
add wave -noupdate -expand -group Top -expand -group Scrambler -color Gold -radix binary /gearbox_scrambler_tb/scr/sync_info
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /gearbox_scrambler_tb/tx_gb/data32
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /gearbox_scrambler_tb/tx_gb/data66
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} -radix decimal /gearbox_scrambler_tb/tx_gb/counter
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /gearbox_scrambler_tb/tx_gb/data_next
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /gearbox_scrambler_tb/tx_gb/rst
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /gearbox_scrambler_tb/tx_gb/gearbox_en
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /gearbox_scrambler_tb/tx_gb/gearbox_rdy
add wave -noupdate -expand -group Top -expand -group {Tx Gearbox} -color {Cornflower Blue} /gearbox_scrambler_tb/tx_gb/buffer_132
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /gearbox_scrambler_tb/rx_gb/data32
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /gearbox_scrambler_tb/rx_gb/data66
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} -radix decimal /gearbox_scrambler_tb/rx_gb/counter
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /gearbox_scrambler_tb/rx_gb/data_valid
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /gearbox_scrambler_tb/rx_gb/rst
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /gearbox_scrambler_tb/rx_gb/gearbox_en
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /gearbox_scrambler_tb/rx_gb/gearbox_rdy
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /gearbox_scrambler_tb/rx_gb/buffer_128
add wave -noupdate -expand -group Top -expand -group {Rx Gearbox} -color {Medium Aquamarine} /gearbox_scrambler_tb/rx_gb/buffer_pos
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /gearbox_scrambler_tb/uns/data_in
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /gearbox_scrambler_tb/uns/data_out
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /gearbox_scrambler_tb/uns/poly
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /gearbox_scrambler_tb/uns/descrambler
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /gearbox_scrambler_tb/uns/enable
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange /gearbox_scrambler_tb/uns/rst
add wave -noupdate -expand -group Top -expand -group Descrambler -color Orange -radix binary /gearbox_scrambler_tb/uns/sync_info
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {39 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 227
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {994 ps}
