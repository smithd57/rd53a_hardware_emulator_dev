vlib work

vlog -sv -work work ../src/on_chip/gearbox_66_to_32.sv
vlog -work work ../src/off_chip/gearbox_32_to_66.v

# Using RD53A Scrambler and testing against custom Descrmabler
#vlog -work work ../src/scrambler_64b_58_39_1.sv

vlog -work work ../src/scrambler.v

vlog -work work ../src/block_sync.v

vlog -work work ./system_test_block_sync_tb.sv

vsim -t 1ps -novopt system_test_block_sync_tb

view signals
view wave

do wave_system_test_block_sync.do

run -all