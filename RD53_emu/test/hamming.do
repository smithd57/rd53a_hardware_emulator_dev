vlib work

vlog -work work ../src/on_chip/hamming_generator.v
vlog -work work hamming_generator_tb.v

vsim -t 1pS -novopt hamming_generator_tb

view signals
view wave

do wave_hamming.do

run 1nS