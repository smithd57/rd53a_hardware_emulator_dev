onerror {resume}
quietly virtual function -install /BloomFilter_tb -env /BloomFilter_tb { &{/BloomFilter_tb/meta, /BloomFilter_tb/region }} testData_sub_i
quietly virtual function -install /BloomFilter_tb -env /BloomFilter_tb { &{/BloomFilter_tb/region[31], /BloomFilter_tb/region[30], /BloomFilter_tb/region[29], /BloomFilter_tb/region[28], /BloomFilter_tb/region[27], /BloomFilter_tb/region[26], /BloomFilter_tb/region[25], /BloomFilter_tb/region[24], /BloomFilter_tb/region[23], /BloomFilter_tb/region[22], /BloomFilter_tb/region[21], /BloomFilter_tb/region[20], /BloomFilter_tb/region[19], /BloomFilter_tb/region[18], /BloomFilter_tb/region[17], /BloomFilter_tb/region[16] }} regAddr
quietly WaveActivateNextPane {} 0
add wave -noupdate -radixshowbase 0 /BloomFilter_tb/clk
add wave -noupdate /BloomFilter_tb/rst
add wave -noupdate -radix hexadecimal -radixshowbase 0 /BloomFilter_tb/regAddr
add wave -noupdate -label SkipData -radixshowbase 0 {/BloomFilter_tb/meta[3]}
add wave -noupdate /BloomFilter_tb/clear
add wave -noupdate -radixshowbase 0 /BloomFilter_tb/write
add wave -noupdate -radixshowbase 0 /BloomFilter_tb/seen
add wave -noupdate -label expectSeen {/BloomFilter_tb/meta[1]}
add wave -noupdate -label perfectSeen -radixshowbase 0 {/BloomFilter_tb/meta[0]}
add wave -noupdate -radix unsigned -radixshowbase 0 /BloomFilter_tb/falsePositives
add wave -noupdate -radix unsigned -radixshowbase 0 /BloomFilter_tb/expectedFalsePositives
add wave -noupdate -divider Internals
add wave -noupdate -radix unsigned -radixshowbase 0 /BloomFilter_tb/dut/hash1
add wave -noupdate -radix unsigned -radixshowbase 0 /BloomFilter_tb/dut/hash2
add wave -noupdate -radix unsigned -radixshowbase 0 /BloomFilter_tb/dut/hash3
add wave -noupdate -radix binary -radixshowbase 0 /BloomFilter_tb/dut/seen1
add wave -noupdate -radix binary -radixshowbase 0 /BloomFilter_tb/dut/seen2
add wave -noupdate -radix binary -radixshowbase 0 /BloomFilter_tb/dut/seen3
add wave -noupdate /BloomFilter_tb/dut/mem
add wave -noupdate -divider {Other TB Data}
add wave -noupdate /BloomFilter_tb/meta
add wave -noupdate /BloomFilter_tb/region
add wave -noupdate /BloomFilter_tb/testData_sub_i
add wave -noupdate -radix unsigned -radixshowbase 0 /BloomFilter_tb/i
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {164753 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 62
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {2563478 ps}
