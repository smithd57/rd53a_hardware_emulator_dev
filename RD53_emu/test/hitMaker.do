vlib work

vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/triggerFifo_1/triggerFifo_sim_netlist.vhdl
vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/triggerTagFifo/triggerTagFifo_sim_netlist.vhdl 
vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/hitDataFIFO_1/hitDataFIFO_sim_netlist.vhdl
vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/RegionDuplexingFIFO/RegionDuplexingFIFO_sim_netlist.vhdl

vlog -work work ../src/on_chip/hitMaker.v
vlog -work work ../src/on_chip/hitMaker2.sv
vlog -work work ../src/on_chip/updatedPRNG.v
vlog -work work ../src/on_chip/simplePRNG.sv
vlog -work work ./hitMaker_tb.v
vlog -work work ../src/on_chip/clusters.sv
vlog -work work ../src/on_chip/clusterHit.sv
vlog -work work ../src/on_chip/lineHit.sv
vlog -work work ../src/on_chip/tonys_hits.sv
vlog -work work ../src/on_chip/updatedPRNG.v
vlog -work work ../src/on_chip/bloomFilter.sv
vlog -work work ../src/on_chip/SAXHash16bit.sv

vsim -t 1ps -novopt work.hitMaker_tb -L unisim

view signals
view wave

do wave_hitMaker.do

run 10 ms
