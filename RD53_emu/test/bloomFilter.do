vlib work

vlog -work work ../src/on_chip/SAXHash16bit.sv
vlog -work work ../src/on_chip/bloomFilter.sv
vlog -work work ./bloomFilter_tb.sv

vsim -t 1ps -novopt work.BloomFilter_tb

view signals
view wave

do wave_bloomFilter.do

run 10 ms
