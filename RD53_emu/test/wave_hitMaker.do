onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /hitMaker_tb/rst
add wave -noupdate -expand -group TB /hitMaker_tb/calSig
add wave -noupdate -expand -group TB /hitMaker_tb/empty
add wave -noupdate -expand -group TB /hitMaker_tb/emptyTT
add wave -noupdate -expand -group TB /hitMaker_tb/full
add wave -noupdate -expand -group TB /hitMaker_tb/hClk
add wave -noupdate -expand -group TB -radix hexadecimal /hitMaker_tb/hitData
add wave -noupdate -expand -group TB /hitMaker_tb/next_hit
add wave -noupdate -expand -group TB -radix hexadecimal /hitMaker_tb/seed1
add wave -noupdate -expand -group TB -radix hexadecimal /hitMaker_tb/seed2
add wave -noupdate -expand -group TB /hitMaker_tb/tClk
add wave -noupdate -expand -group TB /hitMaker_tb/triggerClump
add wave -noupdate -expand -group TB /hitMaker_tb/writeT
add wave -noupdate -expand -group DUT /hitMaker_tb/dut/doWriteT
add wave -noupdate -expand -group DUT /hitMaker_tb/dut/step
add wave -noupdate -expand -group DUT /hitMaker_tb/dut/doAStep
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/tClk
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/hClk
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/dClk
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/rst
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/emptyTT
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/writeT
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/calSig
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/triggerClump
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/triggerTag
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/seed1
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/seed2
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/next_hit
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/update_output_i
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/hitDataConfig
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/hitData
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/full
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/empty
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/hitData_empty
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/doAStep
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/sawAnEmpty
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/step
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/maskedData
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/holdDataFull
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/doWriteT
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/doRead
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/done
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/trigger
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/trigger_r
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/iTriggerClump
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/first_rd_en
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/wait_period
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/valid_step
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/tagInfoIn
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/tagInfoOut
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/tagID
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/BCID
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/tagCounter
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/tagID_r
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/tagInfoOut_r
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/BCID_r
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/trigger_info_i
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/empty_r
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/full1
add wave -noupdate -expand -group hitMaker /hitMaker_tb/dut/empty1
add wave -noupdate /hitMaker_tb/dut/hitGenerator/FIFO_GENERATE_THRESHOLD
add wave -noupdate /hitMaker_tb/dut/hitGenerator/PAD_HIT
add wave -noupdate /hitMaker_tb/dut/hitGenerator/clk_i
add wave -noupdate /hitMaker_tb/dut/hitGenerator/clk2x_i
add wave -noupdate /hitMaker_tb/dut/hitGenerator/rst_i
add wave -noupdate /hitMaker_tb/dut/hitGenerator/trigger_i
add wave -noupdate /hitMaker_tb/dut/hitGenerator/update_output_i
add wave -noupdate /hitMaker_tb/dut/hitGenerator/trigger_info_i
add wave -noupdate /hitMaker_tb/dut/hitGenerator/config_reg_i
add wave -noupdate /hitMaker_tb/dut/hitGenerator/trigger_data_o
add wave -noupdate /hitMaker_tb/dut/hitGenerator/done_o
add wave -noupdate /hitMaker_tb/dut/hitGenerator/randDataClusterHit
add wave -noupdate /hitMaker_tb/dut/hitGenerator/randComp
add wave -noupdate /hitMaker_tb/dut/hitGenerator/randDataLineHit
add wave -noupdate /hitMaker_tb/dut/hitGenerator/newXYO
add wave -noupdate /hitMaker_tb/dut/hitGenerator/useLineType
add wave -noupdate /hitMaker_tb/dut/hitGenerator/startClusterHit
add wave -noupdate /hitMaker_tb/dut/hitGenerator/doneClusterHit
add wave -noupdate /hitMaker_tb/dut/hitGenerator/startLineHit
add wave -noupdate /hitMaker_tb/dut/hitGenerator/doneLineHit
add wave -noupdate /hitMaker_tb/dut/hitGenerator/seenBloom
add wave -noupdate /hitMaker_tb/dut/hitGenerator/writeBloom
add wave -noupdate /hitMaker_tb/dut/hitGenerator/clearBloom
add wave -noupdate /hitMaker_tb/dut/hitGenerator/fifoRead
add wave -noupdate /hitMaker_tb/dut/hitGenerator/fifoWrite
add wave -noupdate /hitMaker_tb/dut/hitGenerator/fifoEmpty
add wave -noupdate /hitMaker_tb/dut/hitGenerator/clusterHitDelimiter
add wave -noupdate /hitMaker_tb/dut/hitGenerator/clusterRegion
add wave -noupdate /hitMaker_tb/dut/hitGenerator/bloomRegion
add wave -noupdate /hitMaker_tb/dut/hitGenerator/fifoIn
add wave -noupdate /hitMaker_tb/dut/hitGenerator/regionsLineHit
add wave -noupdate /hitMaker_tb/dut/hitGenerator/fifoOut
add wave -noupdate /hitMaker_tb/dut/hitGenerator/fifoUsage
add wave -noupdate /hitMaker_tb/dut/hitGenerator/randomRegionAddr
add wave -noupdate /hitMaker_tb/dut/hitGenerator/padOddCluster_UNUSED
add wave -noupdate /hitMaker_tb/dut/hitGenerator/hitReady
add wave -noupdate /hitMaker_tb/dut/hitGenerator/nhitReady
add wave -noupdate /hitMaker_tb/dut/hitGenerator/regionParity
add wave -noupdate /hitMaker_tb/dut/hitGenerator/state
add wave -noupdate /hitMaker_tb/dut/hitGenerator/nextState
add wave -noupdate /hitMaker_tb/dut/hitGenerator/triggerInfoSaved
add wave -noupdate /hitMaker_tb/dut/hitGenerator/ps
add wave -noupdate /hitMaker_tb/dut/hitGenerator/ns
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {62015896 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 178
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {61099555 ps} {65375813 ps}
