onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group {TB Clocks} /emulator_tb/clk160
add wave -noupdate -group {TB Clocks} /emulator_tb/clk40
add wave -noupdate -group {TB Clocks} /emulator_tb/clk400
add wave -noupdate -group {TB Clocks} /emulator_tb/clk640
add wave -noupdate -group {TB Clocks} /emulator_tb/clk1280
add wave -noupdate /emulator_tb/Emulator/clk160
add wave -noupdate /emulator_tb/Emulator/clk40
add wave -noupdate /emulator_tb/Emulator/clk640
add wave -noupdate /emulator_tb/Emulator/rst
add wave -noupdate -color Yellow -label {Data to TTC} -radix hexadecimal -childformat {{{/emulator_tb/datareg[15]} -radix hexadecimal} {{/emulator_tb/datareg[14]} -radix hexadecimal} {{/emulator_tb/datareg[13]} -radix hexadecimal} {{/emulator_tb/datareg[12]} -radix hexadecimal} {{/emulator_tb/datareg[11]} -radix hexadecimal} {{/emulator_tb/datareg[10]} -radix hexadecimal} {{/emulator_tb/datareg[9]} -radix hexadecimal} {{/emulator_tb/datareg[8]} -radix hexadecimal} {{/emulator_tb/datareg[7]} -radix hexadecimal} {{/emulator_tb/datareg[6]} -radix hexadecimal} {{/emulator_tb/datareg[5]} -radix hexadecimal} {{/emulator_tb/datareg[4]} -radix hexadecimal} {{/emulator_tb/datareg[3]} -radix hexadecimal} {{/emulator_tb/datareg[2]} -radix hexadecimal} {{/emulator_tb/datareg[1]} -radix hexadecimal} {{/emulator_tb/datareg[0]} -radix hexadecimal}} -subitemconfig {{/emulator_tb/datareg[15]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[14]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[13]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[12]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[11]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[10]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[9]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[8]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[7]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[6]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[5]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[4]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[3]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[2]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[1]} {-color Yellow -height 16 -radix hexadecimal} {/emulator_tb/datareg[0]} {-color Yellow -height 16 -radix hexadecimal}} /emulator_tb/datareg
add wave -noupdate /emulator_tb/datareg
add wave -noupdate -color Yellow -label {TTC Data} /emulator_tb/ttc_data
add wave -noupdate /emulator_tb/ttc_data
add wave -noupdate -color Yellow -label {CLK 80} /emulator_tb/Emulator/cout_i/clk80
add wave -noupdate -color Cyan -label {Trigger Out} /emulator_tb/Emulator/trig_out
add wave -noupdate -group {Chip Out} /emulator_tb/Emulator/data_next
add wave -noupdate -group {Chip Out} -label {data_next OR} /emulator_tb/Emulator/cout_i/data_next
add wave -noupdate -group {Chip Out} /emulator_tb/Emulator/cout_i/frame_out
add wave -noupdate -group {Chip Out} /emulator_tb/Emulator/cout_i/service_frame
add wave -noupdate -group hitMaker -radix hexadecimal /emulator_tb/Emulator/cout_i/hit_gen/hitData
add wave -noupdate -group hitMaker /emulator_tb/Emulator/cout_i/hit_gen/next_hit
add wave -noupdate -group hitMaker /emulator_tb/Emulator/cout_i/hit_gen/hitData_empty
add wave -noupdate -group hitMaker /emulator_tb/Emulator/cout_i/hit_gen/empty
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/rst
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/clk160
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/clk80
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/next_hit
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/hitin
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/wr_cmd
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/cmdin
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/wr_adx
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/adxin
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/hitData_empty
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/service_counter
add wave -noupdate -group Command_out -radix unsigned /emulator_tb/Emulator/cout_i/cmd_handler_i/cb_wait_cnt
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/cmd_full
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/adx_full
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/data_out
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/data_out_valid
add wave -noupdate -group Command_out /emulator_tb/Emulator/cout_i/cmd_handler_i/service_frame
add wave -noupdate -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/state
add wave -noupdate -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/rst
add wave -noupdate -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/clk80
add wave -noupdate -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/frame
add wave -noupdate -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/service_frame
add wave -noupdate -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/frame_valid
add wave -noupdate -group frame_buffer /emulator_tb/Emulator/cout_i/buffer_i/present_frame
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/clk_i
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/reset_i
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/valid_i
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/data_i
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/ready_o
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/valid_o
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/data_o
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/yumi_cnt_i
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/clk_i
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/reset_i
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/valid_i
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/data_i
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/ready_o
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/valid_o
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/data_o
add wave -noupdate -group frame_buffer -group data_buf /emulator_tb/Emulator/cout_i/buffer_i/data_s_pout/yumi_cnt_i
add wave -noupdate -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/clk_i
add wave -noupdate -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/reset_i
add wave -noupdate -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/valid_i
add wave -noupdate -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/data_i
add wave -noupdate -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/ready_o
add wave -noupdate -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/valid_o
add wave -noupdate -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/data_o
add wave -noupdate -group frame_buffer -group ser_buf\\ /emulator_tb/Emulator/cout_i/buffer_i/ser_s_pout/yumi_cnt_i
add wave -noupdate -group frame_buffer -radix hexadecimal -childformat {{{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[0]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1]} -radix hexadecimal -childformat {{{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][63]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][62]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][61]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][60]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][59]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][58]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][57]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][56]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][55]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][54]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][53]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][52]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][51]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][50]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][49]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][48]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][47]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][46]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][45]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][44]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][43]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][42]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][41]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][40]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][39]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][38]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][37]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][36]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][35]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][34]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][33]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][32]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][31]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][30]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][29]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][28]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][27]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][26]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][25]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][24]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][23]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][22]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][21]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][20]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][19]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][18]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][17]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][16]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][15]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][14]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][13]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][12]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][11]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][10]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][9]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][8]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][7]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][6]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][5]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][4]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][3]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][2]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][1]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][0]} -radix hexadecimal}}} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[2]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[3]} -radix hexadecimal}} -expand -subitemconfig {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[0]} {-height 16 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1]} {-height 15 -radix hexadecimal -childformat {{{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][63]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][62]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][61]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][60]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][59]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][58]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][57]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][56]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][55]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][54]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][53]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][52]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][51]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][50]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][49]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][48]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][47]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][46]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][45]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][44]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][43]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][42]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][41]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][40]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][39]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][38]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][37]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][36]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][35]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][34]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][33]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][32]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][31]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][30]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][29]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][28]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][27]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][26]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][25]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][24]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][23]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][22]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][21]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][20]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][19]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][18]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][17]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][16]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][15]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][14]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][13]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][12]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][11]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][10]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][9]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][8]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][7]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][6]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][5]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][4]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][3]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][2]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][1]} -radix hexadecimal} {{/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][0]} -radix hexadecimal}}} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][63]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][62]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][61]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][60]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][59]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][58]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][57]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][56]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][55]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][54]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][53]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][52]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][51]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][50]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][49]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][48]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][47]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][46]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][45]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][44]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][43]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][42]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][41]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][40]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][39]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][38]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][37]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][36]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][35]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][34]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][33]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][32]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][31]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][30]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][29]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][28]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][27]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][26]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][25]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][24]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][23]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][22]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][21]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][20]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][19]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][18]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][17]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][16]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][15]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][14]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][13]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][12]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][11]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][10]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][9]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][8]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][7]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][6]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][5]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][4]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][3]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][2]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][1]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[1][0]} {-height 15 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[2]} {-height 16 -radix hexadecimal} {/emulator_tb/Emulator/cout_i/buffer_i/frame_hold[3]} {-height 16 -radix hexadecimal}} /emulator_tb/Emulator/cout_i/buffer_i/frame_hold
add wave -noupdate -group frame_buffer -expand /emulator_tb/Emulator/cout_i/buffer_i/service_hold
add wave -noupdate /emulator_tb/data_out
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[123]}
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[122]}
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[121]}
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[119]}
add wave -noupdate -radix decimal {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[118]}
add wave -noupdate /emulator_tb/Emulator/cout_i/process_cmd_in_i/BCR_C
add wave -noupdate -radix hexadecimal /emulator_tb/Emulator/cout_i/process_cmd_in_i/data_in
add wave -noupdate -childformat {{{/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[122]} -radix decimal} {{/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[121]} -radix decimal} {{/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[118]} -radix decimal}} -subitemconfig {{/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[122]} {-height 16 -radix decimal} {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[121]} {-height 16 -radix decimal} {/emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg[118]} {-height 16 -radix decimal}} /emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/channel_bonded
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/data_valid_cb
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/sync_out_cb
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/data_out_cb
add wave -noupdate -group {Rx Cores} -color {Slate Blue} -expand -subitemconfig {{/emulator_tb/blocksync_out[3]} {-color {Slate Blue} -height 16} {/emulator_tb/blocksync_out[2]} {-color {Slate Blue} -height 16} {/emulator_tb/blocksync_out[1]} {-color {Slate Blue} -height 16} {/emulator_tb/blocksync_out[0]} {-color {Slate Blue} -height 16}} /emulator_tb/blocksync_out
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/gearbox_rdy_rx
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/data_valid
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/data_out
add wave -noupdate -group {Rx Cores} -color {Slate Blue} /emulator_tb/sync_out
add wave -noupdate /emulator_tb/Emulator/cout_i/hit_gen/empty
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/clk160
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/rst
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/datain
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/valid
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/data
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/ila_data_read_o
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/data_concat
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/dataint_array
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/data_i
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/validint
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/valid_i
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/sync_pattern
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/lock_level
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/unlock_level
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/clk
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/rst
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/valid_in
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/datain
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/valid
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/dataout
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/ila_data_read_o
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/valid_ii
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/data_ii
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/valid_i
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/locked_i
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/rst_count
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/rst_all
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/rst_other
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/sync_count
add wave -noupdate -group Sub /emulator_tb/Emulator/ttc_decoder_i/channel_align_i/data_i
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/rst_i}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/clk_i}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/clkhigh_i}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data66tx_i}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/read_o}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/dataout_p}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/dataout_n}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/OFB_o}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data8_o}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/bitslip_i}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data66_s}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data32_s}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data32_valid_s}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/data8_s}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/readandscramb_s}
add wave -noupdate -group {TX_core 3} {/emulator_tb/Emulator/four_lane_tx_core/tx_core[3]/tx_lane/RATIO}
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/tClk
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/hClk
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/dClk
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/rst
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/emptyTT
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/writeT
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/calSig
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/triggerClump
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/triggerTag
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/seed1
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/seed2
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/next_hit
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/update_output_i
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/hitData
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/full
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/empty
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/hitData_empty
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/doAStep
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/sawAnEmpty
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/step
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/maskedData
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/holdDataFull
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/doWriteT
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/doRead
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/done
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/trigger
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/trigger_r
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/iTriggerClump
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/first_rd_en
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/wait_period
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/valid_step
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/tagInfoIn
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/tagInfoOut
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/tagID
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/BCID
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/tagCounter
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/tagID_r
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/tagInfoOut_r
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/BCID_r
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/trigger_info_i
add wave -noupdate -group HitMakerFull /emulator_tb/Emulator/cout_i/hit_gen/empty_r
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/data
add wave -noupdate /emulator_tb/Emulator/ttc_decoder_i/valid
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/auto_read_o
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/BCR
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/BCR_C
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/BCR_comb
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/bcr_count
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/bcr_count_r
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/BCR_S
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/button
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/cal
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/CAL_aux
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/CAL_C
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/cal_comb
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/cal_count
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/cal_count_r
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/CAL_edge
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/CAL_S
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/calauxactive
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/calauxdelay
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/caledgecnt
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/caledgedelay
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/calpulse
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/chip_id
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/clk80
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/cmd
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg_adx
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/config_reg_write
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/counter_write
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/data_in
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/data_in_valid
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/state
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/rdreg_comb
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/data_out
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/data_out_valid
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/dataword
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/e_bcr
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/e_cal
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/e_ecr
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/e_pulse
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/e_rd_reg
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/e_reset
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/e_trigger
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/e_wr_reg
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/ECR
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/ECR_C
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/ECR_comb
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/ecr_count
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/ecr_count_r
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/ECR_S
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/full
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/G_PULSE_C
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/globalpulsecnt
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/NEUTRAL_S
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/noop
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/NOOP_C
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/noop_comb
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/NOOP_S
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/pulse
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/pulse_comb
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/pulse_count
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/pulse_count_r
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/RDREG_C
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/PULSE_S
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/rd_fifo
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/rdreg
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/RDREG_S
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/read_count
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/read_count_r
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/register_address
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/register_address_valid
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/rst
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/state_n
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/state_old
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/state_r
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/state_store
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/sync
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/sync_comb
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/sync_pattern
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/SYNC_S
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/the_count
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/the_type
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/trig
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/trigger_count
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/trigger_count_r
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/valid_out
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/words_left
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/words_store
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/wr_fifo
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/write_count
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/write_count_r
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/write_fifo
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/wrreg
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/WRREG_C
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/wrreg_comb
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/wrreg_r
add wave -noupdate -group CMD_process /emulator_tb/Emulator/cout_i/process_cmd_in_i/WRREG_S
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/buffer_data_r
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/buffer_ser_r
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/clk80
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/frame
add wave -noupdate -expand -group Frame_buf -expand /emulator_tb/Emulator/cout_i/buffer_i/frame_hold
add wave -noupdate -expand -group Frame_buf -expand /emulator_tb/Emulator/cout_i/buffer_i/frame_hold_pre_n
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/frame_valid
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/i
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/IDLE
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/next_data_yumi_r
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/next_ser_yumi_r
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/next_state
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/present_frame
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/present_frame_delay_r
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/present_frame_pulse_n
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/ready_n
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/rst
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/service_frame
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/service_hold
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/state
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/trig_out
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/valid_data_n
add wave -noupdate -expand -group Frame_buf /emulator_tb/Emulator/cout_i/buffer_i/valid_ser_n
add wave -noupdate -group Four_lane_out /emulator_tb/Emulator/four_lane_tx_core/clk40
add wave -noupdate -group Four_lane_out /emulator_tb/Emulator/four_lane_tx_core/clk160
add wave -noupdate -group Four_lane_out /emulator_tb/Emulator/four_lane_tx_core/clk640
add wave -noupdate -group Four_lane_out /emulator_tb/Emulator/four_lane_tx_core/rst
add wave -noupdate -group Four_lane_out /emulator_tb/Emulator/four_lane_tx_core/data_in
add wave -noupdate -group Four_lane_out /emulator_tb/Emulator/four_lane_tx_core/sync
add wave -noupdate -group Four_lane_out /emulator_tb/Emulator/four_lane_tx_core/data_out_p
add wave -noupdate -group Four_lane_out /emulator_tb/Emulator/four_lane_tx_core/data_out_n
add wave -noupdate -group Four_lane_out /emulator_tb/Emulator/four_lane_tx_core/data_next
add wave -noupdate -group Four_lane_out /emulator_tb/Emulator/four_lane_tx_core/OFB_o
add wave -noupdate -group Four_lane_out /emulator_tb/Emulator/four_lane_tx_core/ilaActivate
add wave -noupdate -group Four_lane_out /emulator_tb/Emulator/four_lane_tx_core/data8_o
add wave -noupdate -group Four_lane_out /emulator_tb/Emulator/four_lane_tx_core/bitslip_i
add wave -noupdate -group Four_lane_out /emulator_tb/Emulator/four_lane_tx_core/data_in_full
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/DATA_FRAMES
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/IDLE
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/CB_WAIT
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/sync_pattern
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/rst
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/clk160
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/clk80
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/next_hit
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/hitin
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/wr_cmd
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/cmdin
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/wr_adx
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/adxin
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/hitData_empty
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/cmd_full
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/adx_full
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/data_out
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/data_out_valid
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/service_frame
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/trig_out
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/auto_read_i
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/service_counter
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/adx_wait
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/cmd_wait
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/data_available
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/data_out_prereverse
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/startup_wait_cnt
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/cb_wait_cnt
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/data_out_valid_r
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/hitData_muxed
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/cmd_out
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/adx_out
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/cmd_valid
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/adx_valid
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/rd_cmd
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/rd_adx
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/high
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/sync_flag
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/adx_valid_r
add wave -noupdate -group CMD_out /emulator_tb/Emulator/cout_i/cmd_handler_i/cmd_valid_r
add wave -noupdate /emulator_tb/Emulator/cout_i/buffer_i/ready_d
add wave -noupdate /emulator_tb/Emulator/cout_i/buffer_i/ready_s
add wave -noupdate /emulator_tb/Emulator/cout_i/fifo_data_in_ctf
add wave -noupdate /emulator_tb/Emulator/cout_i/ready_s
add wave -noupdate /emulator_tb/Emulator/cout_i/ready_d
add wave -noupdate /emulator_tb/Emulator/cout_i/fifo_full_ctf
add wave -noupdate /emulator_tb/Emulator/cout_i/fifo_empty_ctf
add wave -noupdate /emulator_tb/Emulator/cout_i/fifo_data_out_ctf
add wave -noupdate /emulator_tb/Emulator/cout_i/ctf_wr_en
add wave -noupdate /emulator_tb/Emulator/cout_i/ctf_rd_en
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {5075705936 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 347
configure wave -valuecolwidth 174
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 fs} {13384153152 fs}
