#!/usr/bin/python3

import traceback, matplotlib, numpy, time, sys
from matplotlib import pyplot as plt
from matplotlib import widgets as matwidgets

# theoretical maximum of 512x512, but RD53A emulator is 192x400
MAX_PIXEL_DIMENSIONS = (192, 400)

f = open('tonys_hits_data.txt', 'r')

outfile = None
if len(sys.argv) == 3 and sys.argv[1] == '-o':
    outfile = open(sys.argv[2], 'w')
elif len(sys.argv) != 1:
    print("Error!")
    print('Usage: display_hits [-o output_file.txt]')
    quit()

runs = []

def q():
    global f
    f.close()
    quit()

currentRun = -1
doneTrigger = False
linei = 0
for line in f.readlines():
    if line.startswith('BeginRun'):
        try:
            run = int(line.split()[1])
        except:
            print('ERROR: invalid BeginRun directive on line %d' % linei)
            q()
        if run != currentRun + 1:
            print('ERROR: BeginRun directives must start at zero and increment by 1 (line %d)' % linei)
            q()
        currentRun = run
        doneTrigger = False
        runs.append([])
    else:
        try:
            # 16 hex chars + newline
            assert len(line) == 17
            if not doneTrigger:
                runs[currentRun].append(line[:8])
                hitDatasRaw = [line[8:16]]
                doneTrigger = True
            else:
                hitDatasRaw = [line[:8], line[8:16]]
            for hitDataRaw in hitDatasRaw:
                data = hitDataRaw[4:] # ToT data for the region
                # address for the region = {6'coreCol, 6'coreRow, 4'regionID}
                address = hitDataRaw[:4]
                coreCol = int(address[:2], 16) >> 2
                coreRow = int(address[1:3], 16) & 0x3F
                regionID = int(address[3], 16)
                # cores are 2x8 regions
                x = coreCol * 2 + (regionID % 2)
                y = coreRow * 8 + (regionID >> 1)
                hitData = (x, y, data)
                runs[currentRun].append(hitData)
        except:
            print('ERROR: invalid hit data on line %d' % linei)
            traceback.print_exc()
            q()
            
    linei += 1
f.close()

'''debug
for i in range(len(runs)):
    print("RUN %d: {" % i)
    print("  trigger: %s" % runs[i][0])
    for j in range(len(runs[i])-1):
        print("    hit @ x=%d, y=%d: %s" % runs[i][j+1])
    print('}')
'''

def run2matrix(run):
    mat = numpy.zeros(MAX_PIXEL_DIMENSIONS)
    for hitData in run[1:]: # skip trigger data
        x, y, tots = hitData
        for i in range(4): # 4 ToTs / pixels per region
            mat[y, x*4 + i] = int(tots[i], 16)
    return mat

class FigureController:
    def __init__(self, runs):
        self.run = 0
        self.runs = runs
        self.fig, self.ax = plt.subplots()
        # space for buttons
        plt.subplots_adjust(bottom=0.2)
        self.ax.set_title("initializing...")
        plt.xlabel('x (pixels; divide by 4 for regions)')
        plt.ylabel('y (pixels = regions)')
        # Correct y-axis direction
        plt.ylim(0, MAX_PIXEL_DIMENSIONS[0]-1)
        self.plotRun()

        BTN_PREV_RECT = plt.axes([0.7, 0.05, 0.1, 0.075])
        BTN_NEXT_RECT = plt.axes([0.81, 0.05, 0.1, 0.075])
        self.bnext = matwidgets.Button(BTN_NEXT_RECT, 'Next')
        self.bnext.on_clicked(self.next)
        self.bprev = matwidgets.Button(BTN_PREV_RECT, 'Prev')
        self.bprev.on_clicked(self.prev)
        
    def next(self, event):
        self.run = (self.run + 1) % len(self.runs)
        self.plotRun()
        plt.draw()
        
    def prev(self, event):
        self.run = (self.run - 1) % len(self.runs)
        self.plotRun()
        plt.draw()
        
    def plotRun(self):
        if len(self.runs[self.run]) == 0:
            self.ax.set_title("HitData from run %d. EMPTY FRAME." % self.run)
            self.ax.imshow(numpy.zeros(MAX_PIXEL_DIMENSIONS))
        else:
            self.ax.imshow(run2matrix(self.runs[self.run]))
            self.ax.set_title("HitData from run %d. Trigger: %s" % (self.run, self.runs[self.run][0]))
            
    def convertRuns(self):
        data = []
        for i in range(len(self.runs)):
            for hitData in self.runs[i][1:]: # skip trigger data
                x, y, tots = hitData
                for j in range(4): # 4 ToTs / pixels per region
                    data.append([y, x*4 + j, i, int(tots[j], 16)])
        return data

def writeRunData(convertedRuns, outFile):
    for dat in convertedRuns:
        outFile.write("%d\t%d\t%d\t%d\n" % tuple(dat))

fc = FigureController(runs)
if outfile != None:
    writeRunData(fc.convertRuns(), outfile)
    outfile.close()

plt.show()
