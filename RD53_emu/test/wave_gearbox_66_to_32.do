onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /gearbox66to32_tb/clk
add wave -noupdate /gearbox66to32_tb/data32
add wave -noupdate /gearbox66to32_tb/data66
add wave -noupdate /gearbox66to32_tb/data66_reverse
add wave -noupdate /gearbox66to32_tb/data_next
add wave -noupdate /gearbox66to32_tb/half_clk160
add wave -noupdate /gearbox66to32_tb/rst
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {36931 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 97
configure wave -valuecolwidth 402
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {31375 ps} {83875 ps}
