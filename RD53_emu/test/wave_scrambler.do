onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group top /scrambler_tb/clk
add wave -noupdate -expand -group top /scrambler_tb/data_in
add wave -noupdate -expand -group top /scrambler_tb/data_out
add wave -noupdate -expand -group top /scrambler_tb/enable
add wave -noupdate -expand -group top /scrambler_tb/result
add wave -noupdate -expand -group top /scrambler_tb/rst
add wave -noupdate -expand -group top /scrambler_tb/sync
add wave -noupdate -expand -group top /scrambler_tb/sync_out
add wave -noupdate -expand -group scrambler -color Gold /scrambler_tb/scr/DataIn
add wave -noupdate -expand -group scrambler -color Gold /scrambler_tb/scr/SyncBits
add wave -noupdate -expand -group scrambler -color Gold /scrambler_tb/scr/Ena
add wave -noupdate -expand -group scrambler -color Gold /scrambler_tb/scr/Clk
add wave -noupdate -expand -group scrambler -color Gold /scrambler_tb/scr/Rst
add wave -noupdate -expand -group scrambler -color Gold /scrambler_tb/scr/DataOut
add wave -noupdate -expand -group scrambler -color Gold /scrambler_tb/scr/scrambled_data
add wave -noupdate -expand -group scrambler -color Gold /scrambler_tb/scr/outvec
add wave -noupdate -expand -group descrambler -color {Cornflower Blue} /scrambler_tb/uns/data_in
add wave -noupdate -expand -group descrambler -color {Cornflower Blue} /scrambler_tb/uns/data_out
add wave -noupdate -expand -group descrambler -color {Cornflower Blue} /scrambler_tb/uns/enable
add wave -noupdate -expand -group descrambler -color {Cornflower Blue} /scrambler_tb/uns/sync_info
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 117
configure wave -valuecolwidth 175
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {315 ps}
