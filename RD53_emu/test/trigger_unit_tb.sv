// Engineer: Lev Kurilenko
// Email: levkur@uw.edu
// Date: 2/21/2018
// Institute: University of Washington

`timescale 1ps/1ps

module trigger_unit_tb;

// Clock and Reset Signals
reg rst;
reg clk80, clk160;
parameter clk80_period = 1250;
parameter clk160_period = 625;

// Trigger Unit Signals
reg trigger, trig_clr;
wire trigger_rdy;
wire [15:0] enc_trig;

// Clocks
always #(clk80_period/2) begin
    clk80 <= ~clk80;
end

always #(clk160_period/2) begin
    clk160 <= ~clk160;
end

// Instantiate modules under test
triggerunit t_unit (
   .rst(rst),
   .clk80(clk80),
   .clk160(clk160),
   .trigger(trigger),
   .trig_clr(trig_clr),
   .trigger_rdy(trigger_rdy),
   .enc_trig(enc_trig)
);

// Initialize signals
initial begin
    rst             <= 1'b0;
    clk80           <= 1'b1;
    clk160          <= 1'b1;
    trigger         <= 1'b0;
    trig_clr        <= 1'b0;
end

// Stimulus
initial begin
    @(posedge clk80)
    rst <= 1'b0;
    trig_clr <= 1'b0;
    @(posedge clk80)
    rst <= 1'b1;
    trig_clr <= 1'b1;
    @(posedge clk80)
    rst <= 1'b0;
    trig_clr <= 1'b0;
    repeat(2) @(posedge clk80)  
    trigger <= 1'b0;
    @(posedge clk80)
    trigger <= 1'b1;
    repeat (3) @(posedge clk80)
    trigger <= 1'b0;
    @(posedge clk80)
    trigger <= 1'b1;
    repeat(15) @(posedge clk80)
    trigger <= 1'b0;
    $stop;
end

endmodule