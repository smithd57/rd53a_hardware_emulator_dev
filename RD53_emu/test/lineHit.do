vlib work

vlog -work work ../src/on_chip/lineHit.sv
vlog -work work ../src/on_chip/clusters.sv
vlog -work work ./lineHit_tb.sv

vsim -t 1ps -novopt work.lineHit_tb

view signals
view wave

do wave_lineHit.do

run 10 ms
