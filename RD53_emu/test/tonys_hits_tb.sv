
`timescale 1ns / 1ps

module tonys_hits_tb ();
   logic clk_i, clk2x_i, rst_i, trigger_i, update_output_i, done_o;
   logic [31:0] trigger_info_i;
   logic [63:0] trigger_data_o;
   logic [15:0] config_reg_i;
   
   tonys_hits dut (.*);

  // set up the clocks (40MHz and 80MHz with synchronized posedges)
  parameter ClockDelayT = 25;
  initial begin
      clk_i <= 0; // 40MHz
      forever #(ClockDelayT/2) clk_i <= ~clk_i;
  end
  initial begin
      clk2x_i <= 1; // 80MHz (posedge in sync with negedge)
      forever #(ClockDelayT/4) clk2x_i <= ~clk2x_i;
  end

   integer i, run_i, file;
   task doRun();
      $fwrite(file, "BeginRun %d\n", run_i);
      $display("%t BeginRun %d", $time, run_i);
       
       trigger_i <= 1'b1; @(posedge clk_i);
       trigger_i <= 1'b0; @(posedge clk_i);

       while (!done_o) begin
	  // write trigger data to the file
	  $fwrite(file, "%016x\n", trigger_data_o);
	  @(posedge clk_i);
       end
       
       // Space out seperate runs a little bit
       @(posedge clk_i);
   endtask // doRun

   task doRunWait();
      doRun();
      repeat (10) @(posedge clk_i);
   endtask
   
   initial begin
    file = $fopen("tonys_hits_data.txt", "w");
    dut.bloomFilter.mem = {default: '0};
    //for (i = 0; i < 2**dut.bloomFilter.STAMP_ADDR_WIDTH; i++) dut.bloomFilter.mem[i] = '0;
    trigger_i = 0; 
    update_output_i = 0;  
    trigger_info_i = 32'hFFFFFFFF;
    config_reg_i = 16'd0;
    rst_i = 1'b1; @(posedge clk_i);
    rst_i = 1'b0; @(posedge clk_i);

    for (run_i = 0; run_i < 20; run_i++) doRunWait();
    config_reg_i = 16'h8000; // should be half empty frames, half non-empty
    for (; run_i < 30; run_i++) doRunWait();
    config_reg_i = 16'hD000; // more empties than not
    for (; run_i < 40; run_i++) doRunWait();

    $stop(); // end the simulation
   end
endmodule // tonys_hits_tb
