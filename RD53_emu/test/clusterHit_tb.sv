
`timescale 1ns / 1ps

// Drop-in for tonys_hits that tests ClusterHit
module ClusterHitTester (
  input logic clk_i, rst_i, // 40 MHz = hclk, fifo reads at 160 MHz = dclk
  input logic trigger_i, // start a trigger
  input logic update_output_i, // don't care
  input logic [31:0] trigger_info_i, // initial trigger info
  input logic [15:0] config_reg,
  output logic [63:0] trigger_data_o, // 2 chunks out
  output logic done_o // we're ready for another trigger
);

   logic start, done;
   logic [31:0] random;
   logic [31:0] region;
   ClusterHit clusterHit (.clk(clk_i), .rst(rst_i), .start, .randin(random[27:0]), .region, .done);
   // Larger LFSR means lower repetition, hence 32 > 28
   updatedPRNG #(32) rng (.clk(clk_i), .reset(rst_i), .load(1'b0), .step(1'b1), .seed(32'hDEADBEEF), .dout(random), .new_data());

   // hacky fifo interface
   logic saveRegion, resetRegions, nextRegions, empty;
   logic [63:0] fifoRegions;
   // hacky testbench fifo
   logic [63:0] regions [63:0]; // 63 sets of 2 regions
   int readi, writei;
   assign fifoRegions = regions[readi];
   assign empty = ((readi+1)*2 == writei); // anticipate empty so it arrives on time
   always @(posedge clk_i) begin
      if (resetRegions) begin
	 readi <= 0;
	 writei <= 0;
      end else if (saveRegion) begin
	 if (writei == 0) begin
	    regions[0] <= {32'd0, region};
	    writei <= 2;
	 end else begin
	    if (writei % 2) begin
	       regions[writei/2] <= {regions[writei/2][63:32], region};
	    end else begin
	       regions[writei/2] <= {region, regions[writei/2][31:0]};
	    end
	    writei <= writei + 1;
	 end
      end else if (nextRegions & ~empty) begin // if (saveRegion)
	 if (writei % 2) begin
	    regions[writei/2] <= {regions[writei/2][63:32], regions[writei/2][63:32]};
	    writei <= writei + 1;
	 end
	 readi <= readi + 1;
      end
   end
   
   logic [31:0] triggerInfoSaved;
   enum {STARTGEN, GENERATING, IDLE, START, WORK} ps, ns;
   always_comb begin
      // Default not done, not starting
      {done_o, start, saveRegion, resetRegions, nextRegions} = '0;
      // Don't infer latches
      trigger_data_o = 'X;

      case (ps)
	STARTGEN: begin
	   done_o = 1;
	   start = done;
	   resetRegions = 1;
	   ns = start ? GENERATING : STARTGEN;
	end
	GENERATING: begin
	   done_o = 1;
	   saveRegion = ~done;
	   ns = done ? IDLE : GENERATING;
	end
	IDLE: begin
	   done_o = 1;
	   ns = trigger_i ? START : IDLE;
	 end
	START: begin
	   trigger_data_o = {triggerInfoSaved, 32'd0} | fifoRegions;
	   nextRegions = 1;
	   ns = empty ? STARTGEN : WORK;
	end
	WORK: begin
	   trigger_data_o = fifoRegions;
	   nextRegions = 1;
	   ns = empty ? STARTGEN : WORK;
	end
      endcase
   end

   always_ff @(posedge clk_i) begin
      triggerInfoSaved <= trigger_i ? trigger_info_i : triggerInfoSaved;
      
      if (rst_i) ps <= STARTGEN;
      else ps <= ns;
   end
endmodule   

module clusterHit_tb ();
   logic clk_i, rst_i, trigger_i, update_output_i, done_o;
   logic [31:0] trigger_info_i;
   logic [63:0] trigger_data_o;
   logic [15:0] config_reg;
   
   ClusterHitTester dut (.*);

  //set up the clock
  parameter ClockDelayT = 25000; // 40 MHz
  initial begin
      clk_i <= 0;
      forever #(ClockDelayT/2) clk_i <= ~clk_i;
  end

   integer i, run_i, file;
   task doRun();
      static int err = 0;
      $fwrite(file, "BeginRun %0d\n", run_i);

       trigger_i <= 1'b1; @(posedge clk_i);
       trigger_i <= 1'b0; @(posedge clk_i);

       while (!done_o) begin
	  // write trigger data to the file
	  $fwrite(file, "%016x\n", trigger_data_o);
	  assert(!(^trigger_data_o === 1'bX)); // error if any bits undefined
	  if (^trigger_data_o === 1'bX) err++;
	  @(posedge clk_i);
	  if (err > 50) $stop;
       end

      // Wait for generation to finish
      while (dut.ps != dut.IDLE) @(posedge clk_i);

       // Space out seperate runs a little bit
       @(posedge clk_i);
      if (err != 0) $stop;
   endtask
   
   initial begin
    file = $fopen("tonys_hits_data.txt", "w");
    trigger_i <= 0; 
    update_output_i <= 0;  
    trigger_info_i <= 32'hFFFFFFFF;
    config_reg <= 16'd0;
    rst_i <= 1'b1; @(posedge clk_i);
    rst_i <= 1'b0; @(posedge clk_i);
      while (dut.ps != dut.IDLE) @(posedge clk_i); // wait for generate to finish

      for (run_i = 0; run_i < 20; run_i++) doRun(); // for displaying the hits
     //for (run_i = 0; run_i < 500000; run_i++) doRun(); // takes 5-10 mins to ModelSim, for hash function validation
    /*config_reg <= 16'h8000; // should be half empty frames, half non-empty
    for (; run_i < 10; run_i++) doRun();
    config_reg <= 16'hD000; // more empties than not
    for (; run_i < 20; run_i++) doRun();
     */
    
    $stop(); // end the simulation
   end
endmodule
