
`timescale 1ns / 1ps

module LineHitTester (
  input logic clk_i, clk2x, rst_i, // 40 MHz = hclk, fifo reads at 160 MHz = dclk, clk2x=80MHz
  input logic trigger_i, // start a trigger
  input logic update_output_i, // don't care
  input logic [31:0] trigger_info_i, // initial trigger info
  input logic [15:0] config_reg,
  output logic [63:0] trigger_data_o, // 2 chunks out
  output logic done_o // we're ready for another trigger
);
   logic lastTrigger;
   logic [31:0] triggerInfoSaved;
   always_ff @(posedge clk_i) begin
      lastTrigger <= trigger_i;
      triggerInfoSaved <= trigger_i ? trigger_info_i : triggerInfoSaved;
   end

   logic [31:0] random;
   logic [63:0] regions;
   LineHit lineHit (.clk(clk2x), .rst(rst_i), .start(trigger_i), .randin(random[27:0]), .regions, .done(done_o));
   // Larger LFSR means lower repetition, hence 32 > 28
   updatedPRNG #(32) rng (.clk(clk2x), .reset(rst_i), .load(1'b0), .step(1'b1), .seed(32'hDEADBEEF), .dout(random), .new_data());

   assign trigger_data_o = {(lastTrigger & ~trigger_i) ? triggerInfoSaved : regions[63:32], regions[31:0]};
endmodule   

module lineHit_tb ();
   logic clk_i, clk2x, rst_i, trigger_i, update_output_i, done_o;
   logic [31:0] trigger_info_i;
   logic [63:0] trigger_data_o;
   logic [15:0] config_reg;
   
   LineHitTester dut (.*);

   //set up the clocks
   parameter ClockDelayT = 25.0; // 40 MHz
   initial begin
      clk_i = 0;
      clk2x = 1; // sync to negedge so that it will sync to posedge next
      forever #(ClockDelayT/4.0) clk2x <= ~clk2x; // 2*40MHz
   end
   initial forever #(ClockDelayT/2.0) clk_i <= ~clk_i;

   integer i, run_i, file;
   task doRun();
      static int err = 0;
      $fwrite(file, "BeginRun %0d\n", run_i);

       trigger_i <= 1'b1; @(posedge clk_i);
       trigger_i <= 1'b0; @(posedge clk_i);

       while (!done_o) begin
	  // write trigger data to the file
	  $fwrite(file, "%016x\n", trigger_data_o);
	  assert(!(^trigger_data_o === 1'bX)); // error if any bits undefined
	  if (^trigger_data_o === 1'bX) err++;
	  @(posedge clk_i);
	  if (err > 50) $stop;
       end

       // Space out seperate runs a little bit
       @(posedge clk_i);
      if (err != 0) $stop;
   endtask
   
   initial begin
      file = $fopen("tonys_hits_data.txt", "w");
      trigger_i <= 0; 
      update_output_i <= 0;  
      trigger_info_i <= 32'hFFFFFFFF;
      config_reg <= 16'd0;
      rst_i <= 1'b1; @(posedge clk_i);
      @(posedge clk_i);
      rst_i <= 1'b0; @(posedge clk_i);

      for (run_i = 0; run_i < 20; run_i++) begin
	 doRun(); // for displaying the hits
      end

      $stop(); // end the simulation
   end
endmodule


module HLine_tb ();
   logic clk, rst, done;
   logic [8:0] x, y, length;
   logic [31:0] region;

   HLine dut (.*);

   initial begin
      clk = 0;
      forever #6.25 clk = ~clk; // 2*40MHz
   end

   int regionCount;
   task doLine();
      rst = 1; @(negedge clk);
      @(negedge clk);
      rst = 0; @(negedge clk);
      assert(~done);
      regionCount = 1;
      while (~done) begin regionCount++; @(negedge clk); end
      assert(regionCount[0] == 1'b1); // # of regions read out needs to be odd
      // Harder to check if length >= 4 pixels because there are 4 pix/region
   endtask

   int i;
   initial begin
      for (i = 0; i < 5; i++) begin
	 y = 9'd4;
	 x = i[8:0];
	 length = 9'd20;
	 doLine();
      end

      for (i = 0; i < 4; i++) begin
	 y = 9'd4;
	 x = i[8:0];
	 length = 9'd19 - (i*2);
	 doLine();
      end

      y = 9'd0;
      x = 9'd0;
      length = 9'd0;
      doLine();

      y = 9'd0;
      x = 9'd400;
      length = 9'd0;
      doLine();

      y = 9'd0;
      x = 9'd400;
      length = 9'd300;
      doLine();

      y = 9'd0;
      x = 9'd400;
      length = 9'd400;
      doLine();

      y = 9'd0;
      x = 9'd420;
      length = 9'd420;
      doLine();

      $stop;
   end
endmodule


module VLine_tb ();
   logic clk, rst, done;
   logic [8:0] x, y, length;
   logic [31:0] region;

   VLine dut (.*);

   initial begin
      clk = 0;
      forever #6.25 clk = ~clk; // 2*40MHz
   end

   int regionCount;
   task doLine();
      rst = 1; @(negedge clk);
      @(negedge clk);
      rst = 0; @(negedge clk);
      assert(~done);
      regionCount = 1;
      while (~done) begin regionCount++; @(negedge clk); end
      assert(regionCount[0] == 1'b1); // # of regions read out needs to be odd
      assert(regionCount >= 4); // Lines are minimum 4 pixels long
   endtask

   int i;
   initial begin
      for (i = 0; i < 5; i++) begin
	 x = 9'd4;
	 y = i[8:0];
	 length = 9'd20;
	 doLine();
      end

      for (i = 0; i < 4; i++) begin
	 x = 9'd4;
	 y = i[8:0];
	 length = 9'd19 - (i*2);
	 doLine();
      end

      x = 9'd0;
      y = 9'd0;
      length = 9'd0;
      doLine();

      x = 9'd0;
      y = 9'd192;
      length = 9'd0;
      doLine();

      x = 9'd0;
      y = 9'd192;
      length = 9'd180;
      doLine();

      x = 9'd0;
      y = 9'd192;
      length = 9'd192;
      doLine();

      x = 9'd0;
      y = 9'd250;
      length = 9'd250;
      doLine();

      x = 9'd0;
      y = 9'd500;
      length = 9'd500;
      doLine();

      // Created a non-continuous line in hit generation
      {length, y, x} = {28'h1524692}[26:0];
      doLine();

      $stop;
   end
endmodule
