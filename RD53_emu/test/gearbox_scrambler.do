vlib work

vlog -sv -work work ../src/on_chip/gearbox_66_to_32.sv
vlog -work work ../src/off_chip/gearbox_32_to_66.v
vlog -work work ../src/scrambler.v

vlog -work work ./gearbox_scrambler_tb.sv

vsim -t 1ps -novopt gearbox_scrambler_tb

view signals
view wave

do wave_gearbox_scrambler.do

run -all