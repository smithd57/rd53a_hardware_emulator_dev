`timescale 1ns/1ps

module rd53_txrx_tb();

// common signals
reg clk160;
reg clk625;
reg clk156p25;
reg rst;

wire [3:0] data_serial_p, data_serial_n;

reg [3:0] data_serial_pattern_p, data_serial_pattern_n;
reg [7:0] data_pattern_p, data_pattern_n;
wire [3:0] data_serial_test_p, data_serial_test_n;
reg source, bitslip;
assign data_serial_test_p = source ? data_serial_p : data_serial_pattern_p;
assign data_serial_test_n = source ? data_serial_n : data_serial_pattern_n;

// tx signals
reg [63:0] data_in;
reg data_in_valid;
reg service_frame;

wire system_halt;
wire full, empty;

// rx signals
wire [63:0] data_out;
wire data_out_valid;
wire service;

parameter half_clk160 = 3.125;
parameter half_clk625 = 0.8;
parameter half_clk156p25 = 3.2;

rd53_tx tx_uut (
    .rst(rst),
    .clk160(clk160),
    .clk625(clk625),
    .clk156p25(clk156p25),
    .data_out(data_in),
    .data_out_valid(data_in_valid),
    .service_frame(service_frame),
    .cmd_out_p(data_serial_p),
    .cmd_out_n(data_serial_n),
    .system_halt(system_halt),
    .full(full),
    .empty(empty)
);

rd53_rx rx_uut (
    .clk625(clk625),
    .clk156p25(clk156p25),
    .rst(rst),
    .data(data_out),
    .data_valid(data_out_valid),
    .service(service),
    .data_in_p(data_serial_test_p),
    .data_in_n(data_serial_test_n),
    .bitslip(bitslip)
);

initial begin
    source <= 1'b0;
    bitslip <= 1'b0;
    data_pattern_p <= 8'b11000011;
    data_pattern_n <= 8'b00111100;
    repeat (10) @(posedge clk156p25);
    repeat (7) begin
        bitslip <= 1'b1;
        @(posedge clk156p25);
        bitslip <= 1'b0;
        @(posedge clk156p25);
    end
    repeat (50) @(posedge clk156p25);
    source <= 1'b1;
end

always @(posedge clk625 or negedge clk625) begin
    data_serial_pattern_p <= {4{data_pattern_p[7]}};
    data_serial_pattern_n <= {4{data_pattern_n[7]}};
    data_pattern_p <= {data_pattern_p[6:0], data_pattern_p[7]};
    data_pattern_n <= {data_pattern_n[6:0], data_pattern_n[7]};
end

initial begin
    clk160 <= 1'b1;
    clk625 <= 1'b1;
    clk156p25 <= 1'b1;
    rst <= 1'b0;
    
    data_in <= 64'b0;
    data_in_valid <= 1'b0;
    service_frame <= 1'b0;
end         

always #(half_clk160) begin
    clk160 <= !clk160;
end

always #(half_clk625) begin
    clk625 <= !clk625;
end

always #(half_clk156p25) begin
    clk156p25 <= !clk156p25;
end    

// Save the values input to the tx side here
// to compare against when they are output on
// the rx side.
reg [63:0] comparison_register [255:0];
reg [7:0] tx_pos, rx_pos;

// Stimulus
initial begin
    @(posedge clk160);
    rst <= 1'b1;
    repeat (4) @(posedge clk160);
    rst <= 1'b0;
    tx_pos <= 8'd0;
    rx_pos <= 8'd0;
    forever @(posedge clk160) begin
        // Add the input data to the register
        comparison_register[tx_pos] <= data_in;
    
        data_in_valid <= !data_in_valid;
        data_in[63:32] <= $urandom();
        data_in[31:0] <= $urandom();
        service_frame <= $urandom();
        
        tx_pos <= tx_pos + 1;
    end
end

parameter offset = 0;  // change this
always @(posedge data_out_valid) begin
    if (comparison_register[rx_pos - offset] != data_out) begin
        $display($time, " Data does not match. data_out: %h, comparison_reg: %h", 
                data_out, comparison_register[rx_pos - offset]);
    end
    rx_pos <= rx_pos + 1;
end

endmodule
