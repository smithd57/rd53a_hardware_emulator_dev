onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix binary -radixshowbase 0 /VLine_tb/dut/clk
add wave -noupdate -radix binary -radixshowbase 0 /VLine_tb/dut/rst
add wave -noupdate -radix hexadecimal -radixshowbase 0 /VLine_tb/dut/region
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/y
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/curY
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/length
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/limitedLength
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/x
add wave -noupdate -radix binary -radixshowbase 0 /VLine_tb/dut/done
add wave -noupdate -radix binary -radixshowbase 0 /VLine_tb/dut/finishedLine
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/curX
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/stop
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/nextStop
add wave -noupdate -radix binary -radixshowbase 0 /VLine_tb/dut/regionParity
add wave -noupdate -radix hexadecimal -radixshowbase 0 /VLine_tb/dut/clusterToT
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/X_MAX
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/Y_MAX
add wave -noupdate -radix hexadecimal -radixshowbase 0 /VLine_tb/dut/HIT
add wave -noupdate -radix hexadecimal -radixshowbase 0 /VLine_tb/dut/PAD_HIT
add wave -noupdate -divider spacing
add wave -noupdate -divider spacing
add wave -noupdate -divider Cluster1x1
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/vlineClust1x1/x
add wave -noupdate -radix unsigned -childformat {{{/VLine_tb/dut/vlineClust1x1/y[8]} -radix unsigned} {{/VLine_tb/dut/vlineClust1x1/y[7]} -radix unsigned} {{/VLine_tb/dut/vlineClust1x1/y[6]} -radix unsigned} {{/VLine_tb/dut/vlineClust1x1/y[5]} -radix unsigned} {{/VLine_tb/dut/vlineClust1x1/y[4]} -radix unsigned} {{/VLine_tb/dut/vlineClust1x1/y[3]} -radix unsigned} {{/VLine_tb/dut/vlineClust1x1/y[2]} -radix unsigned} {{/VLine_tb/dut/vlineClust1x1/y[1]} -radix unsigned} {{/VLine_tb/dut/vlineClust1x1/y[0]} -radix unsigned}} -radixshowbase 0 -subitemconfig {{/VLine_tb/dut/vlineClust1x1/y[8]} {-height 17 -radix unsigned -radixshowbase 0} {/VLine_tb/dut/vlineClust1x1/y[7]} {-height 17 -radix unsigned -radixshowbase 0} {/VLine_tb/dut/vlineClust1x1/y[6]} {-height 17 -radix unsigned -radixshowbase 0} {/VLine_tb/dut/vlineClust1x1/y[5]} {-height 17 -radix unsigned -radixshowbase 0} {/VLine_tb/dut/vlineClust1x1/y[4]} {-height 17 -radix unsigned -radixshowbase 0} {/VLine_tb/dut/vlineClust1x1/y[3]} {-height 17 -radix unsigned -radixshowbase 0} {/VLine_tb/dut/vlineClust1x1/y[2]} {-height 17 -radix unsigned -radixshowbase 0} {/VLine_tb/dut/vlineClust1x1/y[1]} {-height 17 -radix unsigned -radixshowbase 0} {/VLine_tb/dut/vlineClust1x1/y[0]} {-height 17 -radix unsigned -radixshowbase 0}} /VLine_tb/dut/vlineClust1x1/y
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/vlineClust1x1/subAddr
add wave -noupdate -radix hexadecimal -radixshowbase 0 /VLine_tb/dut/vlineClust1x1/region
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/vlineClust1x1/X_MAX
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/vlineClust1x1/Y_MAX
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/vlineClust1x1/HIT
add wave -noupdate -divider regaddr
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/vlineClust1x1/coords/x
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/vlineClust1x1/coords/xFix
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/vlineClust1x1/coords/y
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/vlineClust1x1/coords/yFix
add wave -noupdate -radix unsigned -radixshowbase 0 /VLine_tb/dut/vlineClust1x1/coords/subAddr
add wave -noupdate -radix hexadecimal -radixshowbase 0 /VLine_tb/dut/vlineClust1x1/coords/addr
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {193504 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 99
configure wave -valuecolwidth 67
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {493983 ps}
