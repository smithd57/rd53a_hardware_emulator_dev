vlib work

vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/cmd_oserdes/cmd_oserdes_sim_netlist.vhdl
vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/pre_gearbox_fifo/pre_gearbox_fifo_sim_netlist.vhdl

vlog -work work ../src/on_chip/rd53_tx.v
vlog -work work ../src/on_chip/gearbox_66_to_32.sv
vlog -work work ../src/scrambler.v

vlog -work work ./rd53_tx_tb.v

vsim -t 1pS -novopt serdes_tb -L unisim -L unifast -L unimacro -L secureip

view signals
view wave

do wave_rd53_tx.do

run 200 ns