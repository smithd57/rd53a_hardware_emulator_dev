vlib work

vlog -work work ../src/on_chip/updatedPRNG.v
vlog -work work ../src/on_chip/clusters.sv
vlog -work work ../src/on_chip/clusterHit.sv
vlog -work work ./clusterHit_tb.sv

vsim -t 1ps -novopt work.clusterHit_tb

view signals
view wave

do wave_clusterHit.do

run -all
