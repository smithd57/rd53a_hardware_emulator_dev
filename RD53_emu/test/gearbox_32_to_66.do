vlib work

vlog -work work ../src/off_chip/gearbox_32_to_66.v
vlog -work work ./gearbox_32_to_66_tb.v

vsim -t 1ps -novopt gearbox32to66_tb

view signals
view wave

do wave_gearbox_32_to_66.do

run -all