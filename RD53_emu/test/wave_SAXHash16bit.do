onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /SAXHash16bit_tb/data
add wave -noupdate /SAXHash16bit_tb/seed
add wave -noupdate /SAXHash16bit_tb/hash
add wave -noupdate /SAXHash16bit_tb/i
add wave -noupdate -divider Internal
add wave -noupdate /SAXHash16bit_tb/dut/first
add wave -noupdate /SAXHash16bit_tb/dut/data
add wave -noupdate /SAXHash16bit_tb/dut/seed
add wave -noupdate /SAXHash16bit_tb/dut/hash
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {6553 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 97
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {262500 ps}
