vlib work

vlog -work work ../src/on_chip/clusters.sv
vlog -work work ../src/on_chip/clusterHit.sv
vlog -work work ../src/on_chip/lineHit.sv
vlog -work work ../src/on_chip/tonys_hits.sv
vlog -work work ../src/on_chip/updatedPRNG.v
vlog -work work ../src/on_chip/bloomFilter.sv
vlog -work work ../src/on_chip/SAXHash16bit.sv

vcom -work work ../RD53_Emu/RD53_Emulation.runs/RegionDuplexingFIFO_synth_1/RegionDuplexingFIFO_sim_netlist.vhdl
vlog -work work ./tonys_hits_tb.sv

vsim -t 1ps -novopt -L unisim work.tonys_hits_tb

view signals
view wave

do wave_tonys_hits.do

run -all
