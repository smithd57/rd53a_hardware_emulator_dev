onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /lineHit_tb/dut/lineHit/rst
add wave -noupdate /lineHit_tb/dut/clk_i
add wave -noupdate /lineHit_tb/dut/clk2x
add wave -noupdate /lineHit_tb/dut/lineHit/start
add wave -noupdate /lineHit_tb/dut/lineHit/done
add wave -noupdate -divider lineHit
add wave -noupdate /lineHit_tb/dut/lineHit/clk
add wave -noupdate /lineHit_tb/dut/lineHit/rst
add wave -noupdate /lineHit_tb/dut/lineHit/start
add wave -noupdate /lineHit_tb/dut/lastTrigger
add wave -noupdate /lineHit_tb/dut/lineHit/randin
add wave -noupdate /lineHit_tb/dut/lineHit/done
add wave -noupdate /lineHit_tb/dut/lineHit/lineType
add wave -noupdate /lineHit_tb/dut/lineHit/hdone
add wave -noupdate /lineHit_tb/dut/lineHit/vdone
add wave -noupdate /lineHit_tb/dut/lineHit/hrst
add wave -noupdate /lineHit_tb/dut/lineHit/vrst
add wave -noupdate /lineHit_tb/dut/regions
add wave -noupdate /lineHit_tb/dut/lineHit/hregion
add wave -noupdate /lineHit_tb/dut/lineHit/vregion
add wave -noupdate -divider HLine
add wave -noupdate /lineHit_tb/dut/lineHit/hline/X_MAX
add wave -noupdate /lineHit_tb/dut/lineHit/hline/Y_MAX
add wave -noupdate /lineHit_tb/dut/lineHit/hline/HIT
add wave -noupdate /lineHit_tb/dut/lineHit/hline/PAD_HIT
add wave -noupdate /lineHit_tb/dut/lineHit/hline/clk
add wave -noupdate /lineHit_tb/dut/lineHit/hline/rst
add wave -noupdate -radix unsigned /lineHit_tb/dut/lineHit/hline/x
add wave -noupdate -radix unsigned /lineHit_tb/dut/lineHit/hline/y
add wave -noupdate /lineHit_tb/dut/lineHit/hline/length
add wave -noupdate /lineHit_tb/dut/lineHit/hline/limitedLength
add wave -noupdate /lineHit_tb/dut/lineHit/hline/region
add wave -noupdate /lineHit_tb/dut/lineHit/hline/done
add wave -noupdate /lineHit_tb/dut/lineHit/hline/finishedLine
add wave -noupdate -radix unsigned /lineHit_tb/dut/lineHit/hline/curX
add wave -noupdate -radix unsigned /lineHit_tb/dut/lineHit/hline/curY
add wave -noupdate -radix unsigned /lineHit_tb/dut/lineHit/hline/stop
add wave -noupdate -radix unsigned /lineHit_tb/dut/lineHit/hline/diff
add wave -noupdate -radix unsigned /lineHit_tb/dut/lineHit/hline/nextStop
add wave -noupdate -radix unsigned /lineHit_tb/dut/lineHit/hline/subAddr
add wave -noupdate -radix binary /lineHit_tb/dut/lineHit/hline/totTmp
add wave -noupdate -radix binary /lineHit_tb/dut/lineHit/hline/tot
add wave -noupdate /lineHit_tb/dut/lineHit/hline/regionParity
add wave -noupdate -divider VLine
add wave -noupdate /lineHit_tb/dut/lineHit/vline/X_MAX
add wave -noupdate /lineHit_tb/dut/lineHit/vline/Y_MAX
add wave -noupdate /lineHit_tb/dut/lineHit/vline/PAD_HIT
add wave -noupdate /lineHit_tb/dut/lineHit/vline/HIT
add wave -noupdate /lineHit_tb/dut/lineHit/vline/clk
add wave -noupdate /lineHit_tb/dut/lineHit/vline/rst
add wave -noupdate -radix unsigned /lineHit_tb/dut/lineHit/vline/x
add wave -noupdate -radix unsigned /lineHit_tb/dut/lineHit/vline/y
add wave -noupdate -radix unsigned /lineHit_tb/dut/lineHit/vline/length
add wave -noupdate /lineHit_tb/dut/lineHit/vline/limitedLength
add wave -noupdate /lineHit_tb/dut/lineHit/vline/region
add wave -noupdate /lineHit_tb/dut/lineHit/vline/regionParity
add wave -noupdate /lineHit_tb/dut/lineHit/vline/done
add wave -noupdate /lineHit_tb/dut/lineHit/vline/finishedLine
add wave -noupdate -radix unsigned /lineHit_tb/dut/lineHit/vline/curX
add wave -noupdate -radix unsigned /lineHit_tb/dut/lineHit/vline/curY
add wave -noupdate -radix unsigned /lineHit_tb/dut/lineHit/vline/stop
add wave -noupdate /lineHit_tb/dut/lineHit/vline/clusterToT
add wave -noupdate -divider dut
add wave -noupdate /lineHit_tb/dut/rst_i
add wave -noupdate /lineHit_tb/dut/clk_i
add wave -noupdate /lineHit_tb/dut/trigger_i
add wave -noupdate /lineHit_tb/dut/trigger_info_i
add wave -noupdate /lineHit_tb/dut/config_reg
add wave -noupdate /lineHit_tb/dut/trigger_data_o
add wave -noupdate /lineHit_tb/dut/done_o
add wave -noupdate /lineHit_tb/dut/random
add wave -noupdate /lineHit_tb/dut/regions
add wave -noupdate /lineHit_tb/dut/triggerInfoSaved
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {45841 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 132
configure wave -valuecolwidth 125
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {416194 ps}
