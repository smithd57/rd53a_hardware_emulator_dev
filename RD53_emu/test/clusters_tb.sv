
`timescale 1ns / 1ps

// Drop-in for tonys_hits which just returns every cluster in every orientation in a set pattern
module ClusterTester (
  input logic clk_i, rst_i, // 40 MHz = hclk, fifo reads at 160 MHz = dclk
  input logic trigger_i, // start a trigger
  input logic update_output_i, // don't care
  input logic [31:0] trigger_info_i, // initial trigger info
  output logic [63:0] trigger_data_o, // 2 chunks out
  output logic done_o // we're ready for another trigger
);


   logic [31:0] r1x1;
   Cluster1x1 cluster1x1 (.x(9'd10), .y(9'd10), .region(r1x1));

   // Up to (80+112)=192 possible regions other than the cluster1x1 region
   logic [31:0] regions [191:0];
   logic [191:0] initRegionsInvalid;

   // All 2-bit orientation clusters. (2+3+3+4+4+4)*4=20*4 = 80 regions
   genvar gi;
   generate
      for (gi = 0; gi < 4; gi++) begin : orients4
	 assign initRegionsInvalid[20*gi + 0] = 1'b0;
	 Cluster2x1 cluster2x1 (.x(9'd10 + 9'd10*gi[1:0]), .y(9'd20), .orientation(gi[1:0]),
				.regionA(regions[20*gi]), .regionB(regions[20*gi + 1]), .ignoreB(initRegionsInvalid[20*gi + 1])
				);
	 assign initRegionsInvalid[20*gi + 2] = 1'b0;
	 Cluster3x1 cluster3x1 (.x(9'd10 + 9'd10*gi[1:0]), .y(9'd30), .orientation(gi[1:0]),
				.regionA(regions[20*gi + 2]), .regionB(regions[20*gi + 3]), .regionC(regions[20*gi + 4]), .ignoreB(initRegionsInvalid[20*gi + 3]), .ignoreC(initRegionsInvalid[20*gi + 4])
				);
	 assign initRegionsInvalid[20*gi + 2+3] = 1'b0;
	 Cluster2x2_1z cluster2x2 (.x(9'd10 + 9'd10*gi[1:0]), .y(9'd40), .orientation(gi[1:0]),
				   .regionA(regions[20*gi + 5]), .regionB(regions[20*gi + 6]), .regionC(regions[20*gi + 7]), .ignoreB(initRegionsInvalid[20*gi + 6]), .ignoreC(initRegionsInvalid[20*gi + 7])
				   );
	 assign initRegionsInvalid[20*gi + 2+3+3] = 1'b0;
	 ClusterT clusterT (.x(9'd10 + 9'd10*gi[1:0]), .y(9'd60), .orientation(gi[1:0]),
			    .regionA(regions[20*gi + 8]), .regionB(regions[20*gi + 9]),
			    .regionC(regions[20*gi + 10]), .regionD(regions[20*gi + 11]),
			    .ignoreB(initRegionsInvalid[20*gi + 9]), .ignoreC(initRegionsInvalid[20*gi + 10]), .ignoreD(initRegionsInvalid[20*gi + 11])
			    );
	 assign initRegionsInvalid[20*gi + 2+3+3+4] = 1'b0;
	 ClusterZ clusterZ (.x(9'd10 + 9'd10*gi[1:0]), .y(9'd70), .orientation(gi[1:0]),
			    .regionA(regions[20*gi + 12]), .regionB(regions[20*gi + 13]),
			    .regionC(regions[20*gi + 14]), .regionD(regions[20*gi + 15]),
			    .ignoreB(initRegionsInvalid[20*gi + 13]), .ignoreC(initRegionsInvalid[20*gi + 14]), .ignoreD(initRegionsInvalid[20*gi + 15])
			    );
	 assign initRegionsInvalid[20*gi + 2+3+3+4+4] = 1'b0;
	 ClusterO clusterO (.x(9'd10 + 9'd10*gi[1:0]), .y(9'd80), .orientation(gi[1:0]),
			    .regionA(regions[20*gi + 16]), .regionB(regions[20*gi + 17]),
			    .regionC(regions[20*gi + 18]), .regionD(regions[20*gi + 19]),
			    .ignoreB(initRegionsInvalid[20*gi + 17]), .ignoreC(initRegionsInvalid[20*gi + 18]), .ignoreD(initRegionsInvalid[20*gi + 19])
			    );
      end
   endgenerate
   // All 3-bit orientation clusters. (4+5+5)*8=14*8 = 112 regions
   generate
      for (gi = 0; gi < 8; gi++) begin : orients8
	 assign initRegionsInvalid[14*gi + 80] = 1'b0;
	 ClusterL clusterL (.x(9'd10 + 9'd10*gi[2:0]), .y(9'd100), .orientation(gi[2:0]),
			    .regionA(regions[14*gi + 80]), .regionB(regions[14*gi + 81]),
			    .regionC(regions[14*gi + 82]), .regionD(regions[14*gi + 83]),
			    .ignoreB(initRegionsInvalid[14*gi + 81]), .ignoreC(initRegionsInvalid[14*gi + 82]), .ignoreD(initRegionsInvalid[14*gi + 83])
			    );
	 assign initRegionsInvalid[14*gi + 80+4] = 1'b0;
	 ClusterP clusterP (.x(9'd10 + 9'd10*gi[2:0]), .y(9'd110), .orientation(gi[2:0]),
			    .regionA(regions[14*gi + 84]), .regionB(regions[14*gi + 85]),
			    .regionC(regions[14*gi + 86]), .regionD(regions[14*gi + 87]), .regionE(regions[14*gi + 88]),
			    .ignoreB(initRegionsInvalid[14*gi + 85]), .ignoreC(initRegionsInvalid[14*gi + 86]),
			    .ignoreD(initRegionsInvalid[14*gi + 87]), .ignoreE(initRegionsInvalid[14*gi + 88])
			    );
	 assign initRegionsInvalid[14*gi + 80+4+5] = 1'b0;
	 ClusterF clusterF (.x(9'd10 + 9'd10*gi[2:0]), .y(9'd120), .orientation(gi[2:0]),
			    .regionA(regions[14*gi + 89]), .regionB(regions[14*gi + 90]),
			    .regionC(regions[14*gi + 91]), .regionD(regions[14*gi + 92]), .regionE(regions[14*gi + 93]),
			    .ignoreB(initRegionsInvalid[14*gi + 90]), .ignoreC(initRegionsInvalid[14*gi + 91]),
			    .ignoreD(initRegionsInvalid[14*gi + 92]), .ignoreE(initRegionsInvalid[14*gi + 93])
			    );
      end
   endgenerate
   
   logic [191:0] regionsValid, nextRegionsValid, nextValids;
   logic [$clog2(192)-1:0] indexA, indexB;
   Pick2Valid #(192) regionPicker (.valids(regionsValid), .nextValids, .indexA, .indexB);

   int i, j;
   logic [31:0] triggerInfoSaved;
   enum {IDLE, START, WORK} ps, ns;
   always_comb begin
      // Default not done
      done_o = 0;
      // Don't infer latches
      trigger_data_o = 'X;
      nextRegionsValid = 'X;

      case (ps)
	IDLE: begin
	   done_o = 1;
	   ns = trigger_i ? START : IDLE;
	 end
	START: begin
	   trigger_data_o = {triggerInfoSaved, r1x1};
	   nextRegionsValid = ~initRegionsInvalid;
	   ns = WORK;
	end
	WORK: begin
	   trigger_data_o = {regions[indexA], regions[indexB]};
	   ns = (nextValids == '{1'b0}) ? IDLE : WORK;
	   nextRegionsValid = nextValids;
	end
      endcase
   end

   always_ff @(posedge clk_i) begin
      triggerInfoSaved <= trigger_i ? trigger_info_i : triggerInfoSaved;
      regionsValid <= nextRegionsValid;
      
      if (rst_i) ps <= IDLE;
      else ps <= ns;
   end
endmodule   

module clusters_tb ();
   logic clk_i, rst_i, trigger_i, update_output_i, done_o;
   logic [31:0] trigger_info_i;
   logic [63:0] trigger_data_o;
   logic [15:0] config_reg;
   
   ClusterTester dut (.*);

  //set up the clock
  parameter ClockDelayT = 125;
  initial begin
      clk_i <= 0;
      forever #(ClockDelayT/2) clk_i <= ~clk_i;
  end

   integer i, run_i, file;
   task doRun();
      $fwrite(file, "BeginRun %d\n", run_i);
       
       trigger_i <= 1'b1; @(posedge clk_i);
       trigger_i <= 1'b0; @(posedge clk_i);

       while (!done_o) begin
	  // write trigger data to the file
	  $fwrite(file, "%016x\n", trigger_data_o);
	  @(posedge clk_i);
       end
       
       // Space out seperate runs a little bit
       @(posedge clk_i);
   endtask
   
   initial begin
    file = $fopen("tonys_hits_data.txt", "w");
    trigger_i <= 0; 
    update_output_i <= 0;  
    trigger_info_i <= 32'hFFFFFFFF;
    config_reg <= 16'd0;
    rst_i <= 1'b1; @(posedge clk_i);
    rst_i <= 1'b0; @(posedge clk_i);

    for (run_i = 0; run_i < 2; run_i++) doRun();
    config_reg <= 16'h8000; // should be half empty frames, half non-empty
    for (; run_i < 10; run_i++) doRun();
    config_reg <= 16'hD000; // more empties than not
    for (; run_i < 20; run_i++) doRun();
    
    $stop(); // end the simulation
   end
endmodule
