onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label RST /trigger_tb/rst
add wave -noupdate -label CLK160 /trigger_tb/clk160
add wave -noupdate -label CLK200 /trigger_tb/clk200
add wave -noupdate -label {Data In to Trigger FIFO} -radix hexadecimal /trigger_tb/dut/cout_i/data_in
add wave -noupdate -label {Data In to Trigger FIFO Valid} /trigger_tb/dut/cout_i/word_valid
add wave -noupdate -label {Data Out of Trigger FIFO} -radix hexadecimal /trigger_tb/dut/cout_i/fifo_data
add wave -noupdate -label {Data Out of Trigger FIFO Valid} /trigger_tb/dut/cout_i/fifo_data_valid
add wave -noupdate -label {Trigger Out} /trigger_tb/trig_out
add wave -noupdate /trigger_tb/dut/cout_i/count_trigger_i/trig_done
add wave -noupdate -radix unsigned /trigger_tb/dut/cout_i/count_trigger_i/trigCnt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {7387885 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 344
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 100
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {705344 ps}
