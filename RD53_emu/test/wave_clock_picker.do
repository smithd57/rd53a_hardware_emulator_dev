onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group TB /clock_picker_tb/clk160
add wave -noupdate -expand -group TB /clock_picker_tb/clk40
add wave -noupdate -expand -group TB /clock_picker_tb/clk40a
add wave -noupdate -expand -group TB /clock_picker_tb/clk40b
add wave -noupdate -expand -group TB /clock_picker_tb/clk40c
add wave -noupdate -expand -group TB /clock_picker_tb/clk40d
add wave -noupdate -expand -group TB /clock_picker_tb/ctr
add wave -noupdate -expand -group TB /clock_picker_tb/halfclk160
add wave -noupdate -expand -group TB /clock_picker_tb/phase_sel
add wave -noupdate -expand -group TB /clock_picker_tb/rst
add wave -noupdate -expand -group DUT /clock_picker_tb/dut/clk160
add wave -noupdate -expand -group DUT /clock_picker_tb/dut/clk40
add wave -noupdate -expand -group DUT /clock_picker_tb/dut/clk_div
add wave -noupdate -expand -group DUT /clock_picker_tb/dut/clk_out
add wave -noupdate -expand -group DUT /clock_picker_tb/dut/phase_cnt
add wave -noupdate -expand -group DUT /clock_picker_tb/dut/phase_sel
add wave -noupdate -expand -group DUT /clock_picker_tb/dut/phase_seli
add wave -noupdate -expand -group DUT /clock_picker_tb/dut/rst
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {98 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 195
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {959 ps}
