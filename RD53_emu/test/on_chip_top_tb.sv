`timescale 1ns/1fs

module on_chip_top_tb();

// Define the number of emulators instantiated
parameter   NUM_EMULATORS = 2;

// RD53A Emulator Signals
reg         rst, ttc_data;

wire [ 3:0] cmd_out_p      [0:NUM_EMULATORS-1];
wire [ 3:0] cmd_out_n      [0:NUM_EMULATORS-1];
wire        trig_out       [0:NUM_EMULATORS-1];
wire [ 7:0] led;  

// Aurora Rx Signals
wire [63:0] data_out       [0:NUM_EMULATORS-1] [4];
wire [ 1:0] sync_out       [0:NUM_EMULATORS-1] [4];
wire [ 3:0] blocksync_out  [0:NUM_EMULATORS-1];
wire [ 3:0] gearbox_rdy_rx [0:NUM_EMULATORS-1];
wire [ 3:0] data_valid     [0:NUM_EMULATORS-1];

// Aurora Channel Bonding Signals
wire [63:0] data_out_cb    [0:NUM_EMULATORS-1] [4];
wire [ 1:0] sync_out_cb    [0:NUM_EMULATORS-1] [4];
wire        data_valid_cb  [0:NUM_EMULATORS-1];
wire        channel_bonded [0:NUM_EMULATORS-1];

// Aurora Tx Signals
wire [ 3:0] cmd_out_p_sim, cmd_out_n_sim;
wire [ 3:0] data_next;

// Clocks
parameter   half_clk40_period = 12.5;
parameter   halfclk200 = 2.5;
parameter   clk80_period = 12.5;
parameter   clk160_period = 6.25;
parameter   clk400_period = 2.5;
parameter   clk640_period = 1.5625;
parameter   clk1280_period = 0.78125;

reg         clk40;
reg         clk80;
reg         clk160;
reg         clk400;
reg         clk640;
reg         clk1280;

wire        clk160_n;
wire        clk160_p;
wire        ttc_data_p;
wire        ttc_data_n;

initial begin
rst = 1'b0 ;
#(1*halfclk200)
rst = 1'b1;
#(40*halfclk200)
rst = 1'b0 ;
end

//===================
// Clock Generation
//===================

initial begin
    clk40 = 1'b0;
    clk160 = 1'b0;
    clk400 = 1'b0;
    clk640 = 1'b0;
    clk1280 = 1'b0;
end

// 40 MHz clock
always #(half_clk40_period) begin
    clk40 <= ~clk40;
end

// 160 MHz clock
always #(clk160_period/2) begin
    clk160 <= ~clk160;
end

// 400 MHz clock
always #(clk400_period/2) begin
    clk400 <= ~clk400;
end

// 640 MHz clock
always #(clk640_period/2) begin
    clk640 <= ~clk640;
end

// 1.28 GHz clock
always #(clk1280_period/2) begin
    clk1280 <= ~clk1280;
end

assign clk160_p   =  clk160;
assign clk160_n   = !clk160;

assign ttc_data_p =  ttc_data;
assign ttc_data_n = !ttc_data;


//=========================================================================
//                      RD53A Emulator Instantiation
//=========================================================================

on_chip_top #(NUM_EMULATORS) uut (
   .rst(rst),
   .USER_SMA_CLOCK_P(clk160_p), 
   .USER_SMA_CLOCK_N(clk160_n),
   .ttc_data_p('{NUM_EMULATORS{ttc_data_p}}), 
   .ttc_data_n('{NUM_EMULATORS{ttc_data_n}}),
   .cmd_out_p(cmd_out_p),   
   .cmd_out_n(cmd_out_n),
   .trig_out(trig_out),  
   .led(led),
   .debug()
);


//=========================================================================
//              4 x Aurora Tx Instantiation for SIMULATION ONLY
//=========================================================================
aurora_tx_four_lane four_lane_tx_core(
    .rst(rst),
    .clk40(clk40),
    .clk160(clk160),
    .clk640(clk640),
    //.data_in(frame_out),
    .data_in({64'h7800_0000_0000_0040, 64'h7800_0000_0000_0040, 64'h7800_0000_0000_0040, 64'h7800_0000_0000_0040}),
    .sync({
        2'b10,
        2'b10,
        2'b10,
        2'b10
    }),
    .data_out_p(cmd_out_p_sim),
    .data_out_n(cmd_out_n_sim),
    .data_next(data_next)
);

//=========================================================================
//                      4 x Aurora Rx Instantiation
//=========================================================================
genvar i, j;

generate
   for (i = 0; i < NUM_EMULATORS; i++) begin: rx_inst
       for (j = 0 ; j <= 3 ; j++)
           begin : rx_core
               // Original WITHOUT XAPP
               aurora_rx_top rx_lane (
                   .rst(rst),
                   //.clk40(clk40),
                   //.clk160(clk160),
                   //.clk640(clk640),
                   .clk40(uut.clk40),
                   .clk160(uut.clk160),
                   .clk640(uut.clk640),
                   .data_in_p(cmd_out_p[i][j]),
                   .data_in_n(cmd_out_n[i][j]),
                   //.data_in_p(cmd_out_p_sim[i]),
                   //.data_in_n(cmd_out_n_sim[i]),
                   .blocksync_out(blocksync_out[i][j]),
                   .gearbox_rdy(gearbox_rdy_rx[i][j]),
                   .data_valid(data_valid[i][j]),
                   .sync_out(sync_out[i][j]),
                   .data_out(data_out[i][j])
               );
               
               // XAPP included
               // aurora_rx_top_xapp rx_lane (
               //     .rst(rst),
               //     .clk40(clk40),
               //     .clk160(clk160),
               //     .clk640(clk640),
               //     .clk400(clk400),
               //     .data_in_p(cmd_out_p[i]),
               //     .data_in_n(cmd_out_n[i]),
               //     .blocksync_out(blocksync_out[i]),
               //     .gearbox_rdy(gearbox_rdy_rx[i]),
               //     .data_valid(data_valid[i]),
               //     .sync_out(sync_out[i]),
               //     .data_out(data_out[i])
               // );
       end


      // Aurora Channel Bonding
      channel_bond cb (
          .rst(rst),
          .clk40(clk40),
          .data_in(data_out[i]),
          //.data_in({data_out[0], 64'h7800_0000_0000_0042, data_out[2], data_out[3]}),
          .sync_in(sync_out[i]),
          .blocksync_out(blocksync_out[i]),
          .gearbox_rdy_rx(gearbox_rdy_rx[i]),
          .data_valid(data_valid[i]),
          .data_out_cb(data_out_cb[i]),
          .sync_out_cb(sync_out_cb[i]),
          .data_valid_cb(data_valid_cb[i]),
          .channel_bonded(channel_bonded[i])
      );
   end
endgenerate

//=========================================================================
//                      Simulation Stimulus
//=========================================================================

parameter sync_pattern = 16'h817E;
integer counter;
integer other_counter;
reg [15:0] datareg;
reg [63:0] calibrate;
reg [127:0] upnext;

always @(posedge clk160 or posedge rst) begin
   if (rst) begin
      ttc_data <= 1'b0;
      datareg  <= sync_pattern; // For lock
      calibrate <= 48'h6363_6A_6A_6A_A6;  // To calibrate
      // rd_wr cmd, address to read from, some triggers
	   upnext <= 128'h66_66_6A_6A_6A_6A_6A_6A_65_65_6A_6A_2B_2D_6A_6A; // RdReg. send data, no correct symbol pair
	  
      counter  <= 32'd0;
      other_counter <= 32'd0;
   end 
   else begin
      if (counter < 32'd15) begin
         ttc_data <= datareg[15];
         counter  <= counter + 1;
         datareg  <= {datareg[14:0],datareg[15]};
      end
      else begin
         other_counter <= other_counter + 1;
         ttc_data <= datareg[15];
         counter  <= 32'd0;
         if (other_counter < 32'd45) begin
            datareg <= {datareg[14:0],datareg[15]};
         end
         else if (other_counter < 32'd48) begin
            datareg <= calibrate[47:32];
            calibrate <= {calibrate[31:0], calibrate[47:32]};
         end
         else begin
            datareg <= upnext[127:112];
            upnext  <= {upnext[111:0], upnext[127:112]};
         end
      end
   end
end

endmodule