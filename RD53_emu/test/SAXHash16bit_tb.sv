
`timescale 1ns / 1ps

module SAXHash16bit_tb ();
   logic [15:0] data;
   logic [10:0] seed;
   logic [10:0] hash;

   SAXHash16bit dut (.*);

   function int sax16;
      input int A, B, seed, size; // A: 1st byte, B: 2nd B, size: output modulo
      int h;
      h = seed;
      h ^= (h << 5) + (h >> 2) + A;
      h ^= (h << 5) + (h >> 2) + B;
      sax16 = h % 2**size;
   endfunction

   int i;
   task randomTest ();
      data = $urandom_range(2**16-1, 0);
      seed = $urandom_range(2**11-1, 0);
      i = sax16(data[15:8], data[7:0], seed, 11);
      #10;
      assert(hash == i);
      if (hash != i) begin
	 $display("disagreed data(%h) out(%h) comp(%h)", data, hash, i);
      end
      #10;
   endtask
   
   initial begin
      repeat(200) randomTest();
      $stop;
   end
endmodule
