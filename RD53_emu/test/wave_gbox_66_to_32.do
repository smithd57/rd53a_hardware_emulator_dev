onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /gbox66to32_tb/clk
add wave -noupdate /gbox66to32_tb/data32
add wave -noupdate /gbox66to32_tb/data66
add wave -noupdate /gbox66to32_tb/data_next
add wave -noupdate /gbox66to32_tb/half_clk160
add wave -noupdate /gbox66to32_tb/rst
add wave -noupdate /gbox66to32_tb/gearbox_en
add wave -noupdate -expand -group Gearbox -color Orange /gbox66to32_tb/uut/buffer_132
add wave -noupdate -expand -group Gearbox -color Orange -radix decimal /gbox66to32_tb/uut/counter
add wave -noupdate -expand -group Gearbox -color Orange /gbox66to32_tb/uut/upper
add wave -noupdate -expand -group Gearbox -color Orange /gbox66to32_tb/gearbox_rdy
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {453867 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 145
configure wave -valuecolwidth 234
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {419114 ps} {487200 ps}
