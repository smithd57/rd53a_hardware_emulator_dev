vlib work

vlog -work work ../src/on_chip/frame_buffer.sv
vlog -work work ../src/on_chip/frame_incrementer.sv
vlog -work work ../src/on_chip/frame_buffer_four_lane.sv

vlog -work work frame_buffer_four_lane_tb.sv

vsim -t 1ps -novopt frame_buffer_four_lane_tb

view signals
view wave

do wave_frame_buffer_four_lane.do

run 1us