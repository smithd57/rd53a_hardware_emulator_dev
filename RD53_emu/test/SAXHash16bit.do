vlib work

vlog -work work ../src/on_chip/SAXHash16bit.sv
vlog -work work ./SAXHash16bit_tb.sv

vsim -t 1ps -novopt work.SAXHash16bit_tb

view signals
view wave

do wave_SAXHash16bit.do

run 10 ms
