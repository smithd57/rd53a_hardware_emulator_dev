vlib work

# RD53A Modules
vlog -work work ../src/on_chip/bsg_serial_in_parallel_out.sv
vlog -work work ../src/on_chip/SRL.v
vlog -work work ../src/on_chip/shift_align.v
vlog -work work ../src/on_chip/clock_picker.v 
vlog -work work ../src/on_chip/ttc_top.v 
vlog -work work ../src/on_chip/trigger_counter.v
vlog -work work ../src/on_chip/command_process.sv
vlog -work work ../src/on_chip/hitMaker.v
vlog -work work ../src/on_chip/updatedPRNG.v
vlog -work work ../src/on_chip/command_out.v
vlog -work work ../src/on_chip/chip_output.sv
vlog -work work ../src/on_chip/aurora_tx_top.sv
vlog -work work ../src/on_chip/aurora_tx_four_lane.sv
vlog -work work ../src/on_chip/scrambler.v
vlog -work work ../src/on_chip/gearbox_66_to_32.sv
vlog -work work ../src/on_chip/frame_buffer_four_lane.sv
vlog -work work ../src/on_chip/hitMaker2.sv
vlog -work work ../src/on_chip/a_couple_hits1.sv

# Aurora Rx Modules
vlog -work work ../src/aurora/descrambler.v
vlog -work work ../src/aurora/gearbox_32_to_66.v
vlog -work work ../src/aurora/block_sync.v
vlog -work work ../src/aurora/bitslip_fsm.sv
vlog -work work ../src/aurora/channel_bond.sv
vcom -work work ../src/aurora/fifo_fwft_funcsim.vhdl
vlog -work work ../src/aurora/delay_controller_wrap.v
vlog -work work ../src/aurora/serdes_1_to_468_idelay_ddr.v
vlog -work work ../src/aurora/aurora_rx_top_xapp.sv
vlog -work work ../src/aurora/aurora_rx_top.sv

# Aurora Rx ISERDES IP Core
vcom -work work ../src/aurora/cmd_iserdes_funcsim.vhdl
vcom -work work ../src/on_chip/yarr_files/TX/aurora_tx_lane128.vhd
vcom -work work ../src/on_chip/yarr_files/TX/gearbox66to32.vhd
vcom -work work ../src/on_chip/yarr_files/TX/serdes8to1.vhd
vcom -work work ../src/on_chip/yarr_files/TX/serdes32to8.vhd
vcom -work work ../src/on_chip/yarr_files/kintex7/rx-core/aurora_rx_lane.vhd
vcom -work work ../src/on_chip/yarr_files/kintex7/rx-core/gearbox32to66.vhd


# Xilinx IP Cores
vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/fifo_generator_0_1/fifo_generator_0_sim_netlist.vhdl
vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/fifo_generator_1_1/fifo_generator_1_sim_netlist.vhdl
vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/fifo_generator_2_1/fifo_generator_2_sim_netlist.vhdl
vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/triggerFifo_1/triggerFifo_sim_netlist.vhdl
vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/triggerTagFifo/triggerTagFifo_sim_netlist.vhdl 
vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/hitDataFIFO_1/hitDataFIFO_sim_netlist.vhdl
#vcom -work work ../RD53_Emu/RD53_Emulation.runs/cmd_oserdes_synth_1/cmd_oserdes_sim_netlist.vhdl
#vcom -work work ../RD53_Emu/RD53_Emulation.runs/ila_0_synth_1/ila_0_sim_netlist.vhdl
#vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/ila_history/ila_history_sim_netlist.vhdl
#vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/ila_TXlanes/ila_TXlanes_sim_netlist.vhdl
vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/fifo_history_count/fifo_history_count_sim_netlist.vhdl
vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/fifo_history_state/fifo_history_state_sim_netlist.vhdl
vcom -work work ../RD53_Emu/RD53_Emulation.runs/cmd_out_to_frame_buffer_fifo_synth_1/cmd_out_to_frame_buffer_fifo_sim_netlist.vhdl
#vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/ila_rx_dma_wb/ila_rx_dma_wb_sim_netlist.vhdl
#vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/ila_commands/ila_commands_sim_netlist.vhdl
#vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/ilaOut/ilaOut_sim_netlist.vhdl
#vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/ilaHit/ilaHit_sim_netlist.vhdl
#vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/ila_full/ila_full_sim_netlist.vhdl
#vcom -work work ../RD53_Emu/RD53_Emulation.srcs/sources_1/ip/ila_output/ila_output_sim_netlist.vhdl
# vcom -work work ../../project_1_YARR/project_1_YARR_user_files/ip/ila_1/ila_1_sim_netlist.vhdl
# vcom -work work ../RD53_Emulator/RD53_Emulation.srcs/sources_1/ip/vio_0/vio_0_sim_netlist.vhdl

vlog -work work ../src/on_chip/RD53_top.sv
vlog -work work emulator_tb.sv
 
vsim -t 1fs -novopt emulator_tb -L unisim -L secureip -L unifast -L unimacro

view signals
view wave

do wave_emulator.do

#run 12us
run 2400us
