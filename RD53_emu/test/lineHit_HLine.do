vlib work

vlog -work work ../src/on_chip/lineHit.sv
vlog -work work ../src/on_chip/clusters.sv
vlog -work work ./lineHit_tb.sv

vsim -t 1ps -novopt work.HLine_tb

view signals
view wave

do wave_lineHit_HLine.do

run -all
