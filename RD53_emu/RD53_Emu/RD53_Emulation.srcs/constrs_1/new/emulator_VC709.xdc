################################## Clock Constraints ##########################
create_clock -period 6.666 -waveform {0.000 3.333} [get_ports USER_SMA_CLOCK_P]
#create_clock -period 4.000 -waveform {0.000 2.000} [get_ports USER_SMA_CLOCK_P]

#create_generated_clock -name ttc_decoder_i/rclk -source [get_pins pll_fast/clk_out1] -divide_by 3 [get_pins ttc_decoder_i/sample_reg/Q]
#create_generated_clock -name phase_sel_i/clk40_i -source [get_pins ttc_decoder_i/sample_reg/Q] -divide_by 4 [get_pins phase_sel_i/clk_out_reg/Q]
#create_generated_clock -source [get_pins phase_sel_i/clk_out_reg/Q] -multiply_by 2 [get_pins cout_i/recovered_clk/inst/clk_out1]
#create_generated_clock -source [get_pins phase_sel_i/clk_out_reg/Q] -multiply_by 2 [get_pins cout_i/recovered_clk/clk_out1]

################################# Location constraints ########################

##### LOCATIONS ARE FOR XILINX KC705 BOARD ONLY

#Reset input - GPIO_SW_N
#set_property PACKAGE_PIN AA12 [get_ports rst]
#set_property IOSTANDARD LVCMOS15 [get_ports rst]

#set_max_delay -from [get_clocks -of_objects [get_pins pll_fast/inst/mmcm_adv_inst/CLKOUT6]] -to [get_clocks -of_objects [get_pins pll_fast/inst/mmcm_adv_inst/CLKOUT2]] 12.250
#set_max_delay -from [get_clocks -of_objects [get_pins pll_fast/inst/mmcm_adv_inst/CLKOUT2]] -to [get_clocks -of_objects [get_pins pll_fast/inst/mmcm_adv_inst/CLKOUT6]] 12.250

#Sys/Rst Clk - built into board 200MHz
#set_property IOSTANDARD LVDS  [get_ports sysclk_in_n]
#set_property PACKAGE_PIN AD12 [get_ports sysclk_in_p]
#set_property IOSTANDARD LVDS  [get_ports sysclk_in_p]

#USER SMA CLOCK
set_property PACKAGE_PIN AJ32    [get_ports USER_SMA_CLOCK_P]
set_property IOSTANDARD LVCMOS25 [get_ports USER_SMA_CLOCK_P]
set_property PACKAGE_PIN AK32    [get_ports USER_SMA_CLOCK_N]
set_property IOSTANDARD LVDS_25 [get_ports USER_SMA_CLOCK_N]

## OSERDES Output
## USER_GPIO_P
#set_property IOSTANDARD LVDS_25 [get_ports data_out_p]
## USER_GPIO_N
#set_property PACKAGE_PIN Y24 [get_ports data_out_n]
#set_property IOSTANDARD LVDS_25 [get_ports data_out_n]

#LED Ports
#set_property PACKAGE_PIN F16     [get_ports {led[7]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {led[7]}]
#set_property PACKAGE_PIN E18     [get_ports {led[6]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {led[6]}]
#set_property PACKAGE_PIN G19     [get_ports {led[5]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {led[5]}]
#set_property PACKAGE_PIN AE26    [get_ports {led[4]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {led[4]}]
#set_property PACKAGE_PIN AB9     [get_ports {led[3]}]
#set_property IOSTANDARD LVCMOS15 [get_ports {led[3]}]
#set_property PACKAGE_PIN AC9     [get_ports {led[2]}]
#set_property IOSTANDARD LVCMOS15 [get_ports {led[2]}]
#set_property PACKAGE_PIN AA8     [get_ports {led[1]}]
#set_property IOSTANDARD LVCMOS15 [get_ports {led[1]}]
#set_property PACKAGE_PIN AB8     [get_ports {led[0]}]
#set_property IOSTANDARD LVCMOS15 [get_ports {led[0]}]

#Emulator Ports
#HPC_LA_30
set_property PACKAGE_PIN V30    [get_ports ttc_data_p[0]]
set_property PACKAGE_PIN V31    [get_ports ttc_data_n[0]]

#LA_24
set_property PACKAGE_PIN R30    [get_ports ttc_data_p[1]]
set_property PACKAGE_PIN P31    [get_ports ttc_data_n[1]]

#LA_7
set_property PACKAGE_PIN G41    [get_ports ttc_data_p[2]]
set_property PACKAGE_PIN G42    [get_ports ttc_data_n[2]]

#LA_1
set_property PACKAGE_PIN J40    [get_ports ttc_data_p[3]]
set_property PACKAGE_PIN J41    [get_ports ttc_data_n[3]]



set_property IOSTANDARD LVDS_25 [get_ports ttc_data_*]
set_property DIFF_TERM TRUE     [get_ports ttc_data_*]
# CHIP OUTPUT
# Lane 0
# FMC_LPC_LA31_CC_P
set_property PACKAGE_PIN M28   [get_ports cmd_out_p[0][0]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][0]]
# FMC_LPC_LA31_CC_N
set_property PACKAGE_PIN M29   [get_ports cmd_out_n[0][0]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][0]]

# Lane 1
# FMC_LPC_LA29_P
set_property PACKAGE_PIN T29   [get_ports cmd_out_p[0][1]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][1]]
# FMC_LPC_LA29_N
set_property PACKAGE_PIN T30   [get_ports cmd_out_n[0][1]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][1]]

# Lane 2
# FMC_LPC_LA32_CC_P
set_property PACKAGE_PIN V29   [get_ports cmd_out_p[0][2]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][2]]
# N
set_property PACKAGE_PIN U29   [get_ports cmd_out_n[0][2]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][2]]

# Lane 3
# FMC_LPC_LA33_P
set_property PACKAGE_PIN U31   [get_ports cmd_out_p[0][3]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][3]]
# FMC_LPC_LA04_N
set_property PACKAGE_PIN T31   [get_ports cmd_out_n[0][3]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][3]]



# Lane 0
# FMC_LPC_LA23_CC_P
set_property PACKAGE_PIN P30   [get_ports cmd_out_p[1][0]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][0]]
# FMC_LPC_LA23_CC_N
set_property PACKAGE_PIN N31   [get_ports cmd_out_n[1][0]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][0]]

# Lane 1
# FMC_LPC_LA25_P
set_property PACKAGE_PIN K29   [get_ports cmd_out_p[1][1]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][1]]
# FMC_LPC_LA25_N
set_property PACKAGE_PIN K30   [get_ports cmd_out_n[1][1]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][1]]

# Lane 2
# FMC_LPC_LA26_CC_P
set_property PACKAGE_PIN J30   [get_ports cmd_out_p[1][2]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][2]]
# N
set_property PACKAGE_PIN H30   [get_ports cmd_out_n[1][2]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][2]]

# Lane 3
# FMC_LPC_LA27_P
set_property PACKAGE_PIN J31   [get_ports cmd_out_p[1][3]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][3]]
# FMC_LPC_LA27_N
set_property PACKAGE_PIN H31   [get_ports cmd_out_n[1][3]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][3]]


# Lane 0
# FMC_LPC_LA6_CC_P
set_property PACKAGE_PIN K42   [get_ports cmd_out_p[2][0]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][0]]
# FMC_LPC_LA6_CC_N
set_property PACKAGE_PIN J42   [get_ports cmd_out_n[2][0]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][0]]

# Lane 1
# FMC_LPC_LA8_P
set_property PACKAGE_PIN M37   [get_ports cmd_out_p[2][1]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][1]]
# FMC_LPC_LA8_N
set_property PACKAGE_PIN M38   [get_ports cmd_out_n[2][1]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][1]]

# Lane 2
# FMC_LPC_LA9_CC_P
set_property PACKAGE_PIN R42   [get_ports cmd_out_p[2][2]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][2]]
# N
set_property PACKAGE_PIN P42   [get_ports cmd_out_n[2][2]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][2]]

# Lane 3
# FMC_LPC_LA10_P
set_property PACKAGE_PIN N38   [get_ports cmd_out_p[2][3]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][3]]
# FMC_LPC_LA10_N
set_property PACKAGE_PIN M39   [get_ports cmd_out_n[2][3]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][3]]



# Lane 0
# FMC_LPC_LA0_CC_P
set_property PACKAGE_PIN K39   [get_ports cmd_out_p[3][0]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][0]]
# FMC_LPC_LA0_CC_N
set_property PACKAGE_PIN K40   [get_ports cmd_out_n[3][0]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][0]]

# Lane 1
# FMC_LPC_LA2_P
set_property PACKAGE_PIN P41   [get_ports cmd_out_p[3][1]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][1]]
# FMC_LPC_LA2_N
set_property PACKAGE_PIN N41   [get_ports cmd_out_n[3][1]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][1]]

# Lane 2
# FMC_LPC_LA3_CC_P
set_property PACKAGE_PIN M42   [get_ports cmd_out_p[3][2]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][2]]
# N
set_property PACKAGE_PIN L42   [get_ports cmd_out_n[3][2]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][2]]

# Lane 3
# FMC_LPC_LA4_P
set_property PACKAGE_PIN H40   [get_ports cmd_out_p[3][3]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_p[0][3]]
# FMC_LPC_LA4_N
set_property PACKAGE_PIN H41   [get_ports cmd_out_n[3][3]]
#set_property DIFF_TERM TRUE     [get_ports cmd_out_n[0][3]]

set_property IOSTANDARD LVDS [get_ports cmd_out_*]

# FMC LPC TRIG_OUT
#set_property PACKAGE_PIN AA20    [get_ports trig_out[0]]
#set_property IOSTANDARD LVCMOS25 [get_ports trig_out[0]]

## DEBUG
#set_property PACKAGE_PIN AB25    [get_ports {debug[0]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {debug[0]}]
#set_property PACKAGE_PIN AA25    [get_ports {debug[1]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {debug[1]}]
#set_property PACKAGE_PIN AB28    [get_ports {debug[2]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {debug[2]}]
#set_property PACKAGE_PIN AA27    [get_ports {debug[3]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {debug[3]}]

# Set False clock paths
# set_false_path -from [get_pins ttc_decoder_i/posOR_reg_reg/C] -to [get_pins ttc_decoder_i/sample_reg/D]